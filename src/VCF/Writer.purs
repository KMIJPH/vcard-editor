-- File Name: Writer.purs
-- Description: vCard writer
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 29 Apr 2023 15:44:17
-- Last Modified: 06 Nov 2023 15:28:28

module VCF.Writer where

import Prelude

import Data.Array (filter)
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Data.String as S
import Parsing (runParser)
import VCF.Encoding (Encodings(..))
import VCF.Parsers (splitLines, wrapLine)
import VCF.Type.Common (empty, escapeValue, toString)
import VCF.Type.Name (NameOrder(..))
import VCF.Type.Property (Property(..))
import VCF.Type.VCard (VCard(..))

type ExportSettings =
  { encoding :: Maybe Encodings
  , nameOrder :: NameOrder
  , version :: Number
  , wrap :: Boolean
  }

defaultSettings :: ExportSettings
defaultSettings =
  { encoding: Just $ QP
  , nameOrder: Western
  , version: 2.1
  , wrap: true
  }

write :: ExportSettings -> VCard -> String
write settings (VCard v) =
  "BEGIN:VCARD" <> "\r\n"
    <> "VERSION:"
    <> version
    <> (if name == "\r\n" then "" else name)
    <> (if fn == "\r\n" then "" else fn)
    <> (if props == "\r\n" then "" else props)
    <> (if adrs == "\r\n" then "" else adrs)
    <> "END:VCARD"
  where
  version = case v.version of
    2.1 -> "2.1\r\n"
    3.0 -> "3.0\r\n"
    4.0 -> "4.0\r\n"
    _ -> "unknown\r\n"
  name = (toString settings.encoding (removeEmptyParams v.name) <> "\r\n")
  fn = (toString settings.encoding (removeEmptyParams v.fn) <> "\r\n")
  escaped = map
    ( \(Property p) -> Property
        { name: p.name
        , value: escapeValue p.value
        , group: p.group
        , params: p.params
        }
    )
    v.props
  filtered = filter (\(Property p) -> p.value /= empty) escaped
  props =
    ( S.joinWith "\r\n"
        ( flip map filtered \e ->
            case settings.wrap of
              true -> wrap $ toString settings.encoding (removeEmptyParams e)
              false -> toString settings.encoding (removeEmptyParams e)
        ) <> "\r\n"
    )
  adrs = (S.joinWith "\r\n" (map (\e -> toString settings.encoding (removeEmptyParams e)) v.adrs) <> "\r\n")

  -- wrap value if it is too long
  wrap :: String -> String
  wrap input =
    S.joinWith "\r\n" lines
    where
    lines = case runParser input splitLines of
      Right splits -> map
        ( \line -> case S.length line > 74 of
            true -> case runParser line (wrapLine 74) of
              Right res -> res
              Left _ -> line
            false -> line
        )
        splits
      Left _ -> [ input ]

  -- removes empty parameters from a property
  removeEmptyParams :: forall a. Property a -> Property a
  removeEmptyParams (Property p) =
    Property
      { name: p.name
      , value: p.value
      , group: p.group
      , params: filter (\param -> param /= empty) p.params
      }

writeVCards :: Array VCard -> ExportSettings -> String
writeVCards cards settings =
  S.joinWith "\r\n" processed
  where
  processed = flip map cards \e -> write settings e
