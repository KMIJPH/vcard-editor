-- File Name: Parsers.purs
-- Description: Common parsers
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 27 Apr 2023 19:18:00
-- Last Modified: 23 Nov 2023 15:10:43

module VCF.Parsers where

import Prelude

import Control.Alt ((<|>))
import Data.Array (any, concat, fromFoldable, head, length, mapWithIndex, reverse, snoc, tail)
import Data.CodePoint.Unicode (isAscii)
import Data.Either (Either(..))
import Data.Foldable (foldr)
import Data.List (List(..), (:))
import Data.Maybe (Maybe(..), fromMaybe)
import Data.String (splitAt)
import Data.String as String
import Data.String.CodePoints as CP
import Data.String.CodeUnits (fromCharArray, singleton)
import Data.String.Common as SC
import Data.Tuple (Tuple(..))
import Parsing (Parser, runParser)
import Parsing.Combinators (lookAhead, many, manyTill, notFollowedBy, optionMaybe, optional, sepBy)
import Parsing.Combinators.Array as CA
import Parsing.String (anyChar, char, eof, satisfy, string)
import Parsing.String.Basic (noneOf, number, oneOf)
import Parsing.String.Replace (replace)

begin :: Parser String String
begin = string "BEGIN:VCARD"

end :: Parser String String
end = string "END:VCARD"

-- | (un)escaped character
esc :: Char -> Parser String Char
esc c = string ("\\" <> singleton c) *> pure c

-- | escapes a character in a string (if it isn't already)
escape :: String -> Char -> String
escape input c = do
  let str = runParser input (manyTill (esc c <|> anyChar) eof)
  case str of
    Right escaped -> replace (fromCharArray $ fromFoldable escaped) ((\_ -> "\\" <> s) <$> (string s))
    Left _ -> input
  where
  s = singleton c

-- | unescapes a character in a string
unEscape :: String -> Char -> String
unEscape input c = do
  let str = runParser input (manyTill (esc c <|> anyChar) eof)
  case str of
    Right escaped -> fromCharArray $ fromFoldable escaped
    Left _ -> input

-- | escapes all the invalid characters
-- | should be ; in v2.1
-- | v 3.0 adds ,
escapeAll :: String -> String
escapeAll input = do
  escape (escape input ';') ','

-- | unescapes all the invalid characters
unEscapeAll :: String -> String
unEscapeAll input =
  unEscape (unEscape input ';') ','

-- | U+000D (optionally) followed by U+000A
eol :: Parser String Unit
eol = do
  a <- oneOf [ '\r', '\n' ]
  case a of
    '\r' -> do
      optional $ char '\n'
    _ -> pure unit

-- | carriage return line feed
crlf :: Parser String Unit
crlf = do
  _ <- char '\r'
  _ <- char '\n'
  pure unit

space :: Parser String Unit
space = do
  _ <- char ' '
  pure unit

-- | field separator (U+003B)
semicolon :: Parser String Char
semicolon = char ';'

-- | key/value separator (U+003A)
colon :: Parser String Char
colon = char ':'

-- | parameter key/value separator (U+003D)
equals :: Parser String Char
equals = char '='

-- | splits a string (by newlines) into an array
splitLines :: Parser String (Array String)
splitLines = do
  listChars <- sepBy (CA.many $ noneOf [ '\n', '\r' ]) (crlf)
  pure $ fromFoldable $ map (\e -> fromCharArray e) listChars

-- | returns a string up until a certain character
part :: forall a. Show a => Parser String a -> Parser String String
part sep = do
  p <- manyTill (esc '=' *> esc ';' *> esc ':' <|> anyChar) sep
  next <- optionMaybe $ lookAhead $ char ' ' -- line continuation
  case next of
    Nothing -> pure $ fromCharArray $ fromFoldable p
    Just _ -> do
      _ <- char ' ' -- one space in front will be removed
      np <- part sep
      _ <- optional crlf -- some writers put an empty line after a multiline string
      pure $ (fromCharArray $ fromFoldable p) <> "\r\n" <> np

-- | splits string by semicolon
splitPart :: Parser String (Array String)
splitPart = do
  listChars <- sepBy (CA.many (esc ';' <|> noneOf [ ';' ])) semicolon
  pure $ fromFoldable $ map (\e -> fromCharArray e) listChars

-- | the property group (A, B, ...)
propertyGroup :: Parser String String
propertyGroup = do
  n <- anyChar
  _ <- char '.'
  pure $ singleton n

-- | version (any number)
version :: Parser String Number
version = do
  _ <- string "VERSION:"
  v <- number
  _ <- crlf
  pure v

-- | indents newlines (\r\n) with \r\n followed by a space
indentNewlines :: Parser String String
indentNewlines = do
  p <- manyTill anyChar (crlf <|> eof)
  _ <- notFollowedBy space
  next <- optionMaybe $ lookAhead $ anyChar
  case next of
    Nothing -> pure (fromCharArray $ fromFoldable p)
    Just _ -> do
      np <- indentNewlines
      pure $ (fromCharArray $ fromFoldable p) <> "\r\n " <> np

-- | valid ASCII string
isValidAscii :: String -> Boolean
isValidAscii input =
  not $ any (\c -> c == false) booleans
  where
  arr = CP.toCodePointArray input
  booleans = map (\c -> (isAscii c) || (CP.singleton c) == "-") arr

-- | parses words (separated by spaces) into an array
words :: Parser String (Array String)
words = do
  ws <- manyTill (word) (space <|> crlf <|> eof)
  pure $ fromFoldable ws
  where
  word :: Parser String String
  word = do
    w <- many (satisfy (\c -> c /= ' '))
    _ <- optional $ char ' '
    pure $ fromCharArray $ fromFoldable w

-- | folds/wraps a string to lines of n characters max
wrapLine :: Int -> Parser String String
wrapLine maxLength = do
  array <- words
  let separated = mapWithIndex (\i e -> if i /= ((length array) - 1) then e <> " " else e) array
  let list = foldr Cons Nil separated
  pure $ SC.joinWith "\r\n" (fromFoldable (wrapper maxLength [] list))
  where
  -- wraps an array of words to a list of n character long lines
  wrapper :: Int -> Array String -> List String -> List String
  wrapper max array list =
    case list of
      (Nil) -> Nil -- empty
      (w : Nil) -> case splitter max (current <> w) of
        Nothing -> Cons (SC.joinWith "\r\n" (snoc before (current <> w))) Nil
        Just (Tuple fst snd) -> wrapper max (concat [ before, [ fst, " " ] ]) (Cons snd Nil)
      (w : ws) -> case splitter max (current <> w) of
        Nothing -> wrapper max (snoc before (current <> w)) ws
        Just (Tuple fst snd) -> wrapper max (concat [ before, [ fst, " " ] ]) (Cons snd ws)
    where
    rev = reverse array
    current = fromMaybe "" $ head rev
    before = reverse $ fromMaybe [] $ tail rev

  -- splits a string to a maximum line length
  splitter :: Int -> String -> Maybe (Tuple String String)
  splitter max line =
    case String.length line > (max - 1) of
      false -> Nothing
      true -> do
        let split = splitAt max line
        Just (Tuple split.before split.after)
