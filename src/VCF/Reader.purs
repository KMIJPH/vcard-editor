-- File Name: Reader.purs
-- Description: vCard v2.1 reader
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 01 Apr 2023 17:11:51
-- Last Modified: 19 Nov 2023 15:18:18

module VCF.Reader where

import Prelude

import Control.Alt ((<|>))
import Data.Array (concat, filter, length)
import Data.Either (Either(..), fromRight)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Tuple (Tuple(..), fst, snd)
import Parsing (Parser, runParser)
import Parsing.Combinators (manyTill, lookAhead, notFollowedBy, optionMaybe, optional, try)
import Parsing.Combinators.Array (many)
import Parsing.String (eof, rest, anyChar, string)
import Parsing.String.Basic (oneOf)
import VCF.Parsers (begin, colon, crlf, end, equals, part, propertyGroup, splitPart, version)
import VCF.Type.Common (Parameter(..), Value(..), empty, toOther)
import VCF.Type.Property (Property(..))
import VCF.Type.VCard (VCard(..))

-- | a single property parameter
parameter :: String -> Array Parameter
parameter s =
  case runParser s splitPart of
    Left _ -> []
    Right parts ->
      filter (\e -> e /= empty)
        $ map (\e -> fromRight empty (runParser e parseParam)) parts
  where
  parseParam :: Parser String Parameter
  parseParam = do
    param <- (try $ part equals) <|> string ""
    value <- (try rest) <|> string ""
    pure $ Parameter { param: param, value: value }

-- | a property with a known name
known :: String -> Parser String (Property Value)
known property = do
  grouping <- try (propertyGroup) <|> (string "")
  _ <- string property
  paramString <- part colon
  v <- part crlf
  pure $ Property
    { name: property
    , value: Value v
    , group: grouping
    , params: parameter paramString
    }

-- | a property with an unknown name
unknown :: Parser String (Property Value)
unknown = do
  _ <- notFollowedBy end
  property <- lookAhead $ try (propertyGroup *> part (oneOf [ ';', ':' ])) <|> (part (oneOf [ ';', ':' ]))
  known property

-- | unsafe mode can be toggled, which means VERSION and N won't be needed at the top of the vCard
vCard :: Boolean -> Parser String VCard
vCard unsafe = do
  _ <- begin *> crlf
  nameVersion <- case unsafe of
    true -> do
      v <- optionMaybe version
      maybeN <- optionMaybe $ (known "N")
      name <- case maybeN of
        Just n -> pure n
        Nothing -> pure (Property { name: "N", value: Value "", group: "", params: [] })
      pure $ Tuple name (fromMaybe 2.1 v)
    false -> do
      v <- version
      n <- known "N"
      pure $ Tuple n v
  fields <- many unknown
  _ <- manyTill anyChar end

  let validVersion = if snd nameVersion == 4.0 || snd nameVersion == 3.0 then snd nameVersion else 2.1
  let hasName = length (filter (\(Property p) -> p.name == "N") fields) /= 0

  pure $ VCard
    { adrs: []
    , name: empty
    , fn: empty
    , props: if hasName then fields else concat [ [ fst nameVersion ], fields ]
    , version: validVersion
    }

-- | a single vcard
single :: Boolean -> Parser String VCard
single unsafe = do
  card <- (vCard unsafe)
  _ <- optional crlf
  _ <- optional $ end
  pure $ toOther card

-- | nested vcards (groupings with common fields)
nested :: Parser String (Array VCard)
nested = do
  _ <- begin *> crlf
  commonFields <- many common
  vCards <- many (single true)
  _ <- optional crlf
  _ <- optional eof
  let woVersion = filter (\(Property e) -> e.name /= "VERSION") commonFields
  pure $ flip map vCards \(VCard v) ->
    VCard { name: v.name, fn: v.fn, adrs: v.adrs, props: concat [ v.props, woVersion ], version: v.version }
  where
  common :: Parser String (Property Value)
  common = do
    _ <- try $ notFollowedBy begin
    field <- unknown
    pure field
