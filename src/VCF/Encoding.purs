-- File Name: Encoding.purs
-- Description: Encodings (js)
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 01 May 2023 21:02:55
-- Last Modified: 06 Nov 2023 15:55:16

module VCF.Encoding where

foreign import _decQP :: String -> String
foreign import _decBase64 :: String -> String
foreign import _encQP :: String -> String
foreign import _encBase64 :: String -> String

data Encodings = QP | Base64 | EightBit | Binary
