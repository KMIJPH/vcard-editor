-- File Name: Address.purs
-- Description: VCard Address
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 02 May 2023 15:04:52
-- Last Modified: 04 Nov 2023 16:01:38

module VCF.Type.Address where

import Prelude

import Data.Array (toUnfoldable)
import Data.Either (Either(..))
import Data.List (List(Nil), (:))
import Data.Newtype (class Newtype)
import Parsing (runParser)
import VCF.Parsers (escapeAll, splitPart, unEscapeAll)
import VCF.Type.Common
  ( class Empty
  , class FromPrimitive
  , class ToOther
  , class ToString
  , Value(..)
  , empty
  , toOther
  , toString
  , fromPrimitive
  )

newtype Address = Address
  { adr :: String
  , ext :: String
  , str :: String
  , loc :: String
  , reg :: String
  , postal :: String
  , ctry :: String
  }

derive instance eqAdr :: Eq Address

derive instance newtypeAdr :: Newtype Address _

instance showAdr :: Show Address where
  show (Address a) = show a

instance emptyAdr :: Empty Address where
  empty = Address { adr: "", ext: "", str: "", loc: "", reg: "", postal: "", ctry: "" }

instance fromPrimitiveAddress :: FromPrimitive Value Address where
  fromPrimitive encoding value =
    Address
      { adr: unEscapeAll a.adr
      , ext: unEscapeAll a.ext
      , str: unEscapeAll a.str
      , loc: unEscapeAll a.loc
      , reg: unEscapeAll a.reg
      , postal: unEscapeAll a.postal
      , ctry: unEscapeAll a.ctry
      }
    where
    fp :: Value -> Value
    fp v = fromPrimitive encoding v

    address = toOther $ fp value -- this value should be decoded
    fromAddress (Address addr) = addr
    a = fromAddress address

instance toStringAdr :: ToString Address where
  toString encoding address =
    toString encoding (toValue address) -- should take care of encoding etc.
    where
    toValue :: Address -> Value
    toValue addr = toOther addr -- escapes the value

instance toOtherAddressValue :: ToOther Address Value where
  toOther (Address a) =
    case Address a == empty of
      true -> Value ""
      false ->
        Value
          $ escapeAll a.adr
              <> ";"
              <> escapeAll a.ext
              <> ";"
              <> escapeAll a.str
              <> ";"
              <> escapeAll a.loc
              <> ";"
              <> escapeAll a.reg
              <> ";"
              <> escapeAll a.postal
              <> ";"
              <> escapeAll a.ctry

instance toOtherValueAddress :: ToOther Value Address where
  toOther value =
    case runParser (toOther value) splitPart of
      Left _ -> empty
      Right parts ->
        case toUnfoldable parts of
          (p1 : Nil) -> Address { adr: p1, ext: "", str: "", loc: "", reg: "", postal: "", ctry: "" }
          (p1 : p2 : Nil) -> Address { adr: p1, ext: p2, str: "", loc: "", reg: "", postal: "", ctry: "" }
          (p1 : p2 : p3 : Nil) -> Address { adr: p1, ext: p2, str: p3, loc: "", reg: "", postal: "", ctry: "" }
          (p1 : p2 : p3 : p4 : Nil) -> Address { adr: p1, ext: p2, str: p3, loc: p4, reg: "", postal: "", ctry: "" }
          (p1 : p2 : p3 : p4 : p5 : Nil) -> Address { adr: p1, ext: p2, str: p3, loc: p4, reg: p5, postal: "", ctry: "" }
          (p1 : p2 : p3 : p4 : p5 : p6 : Nil) -> Address { adr: p1, ext: p2, str: p3, loc: p4, reg: p5, postal: p6, ctry: "" }
          (p1 : p2 : p3 : p4 : p5 : p6 : p7 : _) -> Address { adr: p1, ext: p2, str: p3, loc: p4, reg: p5, postal: p6, ctry: p7 }
          _ -> empty

