-- File Name: VCard.purs
-- Description: VCard type
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 02 May 2023 10:10:15
-- Last Modified: 21 Dez 2023 12:07:21

module VCF.Type.VCard where

import Prelude

import Data.Array (filter, length, nub, toUnfoldable)
import Data.List (List(Nil), (:))
import Data.Newtype (class Newtype)
import VCF.Encoding (Encodings(..))
import VCF.Type.Address (Address)
import VCF.Type.Common (class Empty, class ToOther, Value(..), fromPrimitive, empty, toOther, unEscapeValue)
import VCF.Type.Name (Name)
import VCF.Type.Property (Property(..))

newtype VCard = VCard
  { adrs :: Array (Property Address)
  , name :: Property Name
  , fn :: Property Value
  , props :: Array (Property Value)
  , version :: Number
  }

derive instance eqVC :: Eq VCard
derive instance newtypeProperty :: Newtype VCard _
instance showVC :: Show VCard where
  show (VCard v) = show v

instance ordVC :: Ord VCard where
  compare (VCard v1) (VCard v2) =
    if name1 < name2 then LT
    else if name1 > name2 then GT
    else EQ
    where
    name :: Property Value -> String
    name (Property p) = toOther p.value

    name1 = name v1.fn
    name2 = name v2.fn

instance emptyVCard :: Empty VCard where
  empty = VCard
    { adrs: []
    , name: empty
    , fn: empty
    , props: []
    , version: 2.1
    }

instance toOtherVCard :: ToOther VCard VCard where
  toOther (VCard vcard) =
    transformN
      (VCard $ vcard { props = nub $ flip map vcard.props \p -> fromPrimitive QP p })
      # generateFN
      # transformAdrs
      # unescapeVCard
    where

    -- transforms the name property to a Property Name in the VCard
    transformN :: VCard -> VCard
    transformN (VCard v) =
      case names of
        (name1 : Nil) -> VCard $ v { name = name1, props = notN }
        (name1 : _) -> VCard $ v { name = name1, props = notN }
        _ -> VCard v
      where
      n = filter (\(Property e) -> e.name == "N") v.props
      n2 = filter (\(Property x) -> x.value /= empty) n
      notN = filter (\(Property e) -> e.name /= "N") v.props
      names = toUnfoldable $ map (\(Property e) -> Property e { name = "N", value = toOther e.value }) n2

    -- generates the FN from the Property Name
    generateFN :: VCard -> VCard
    generateFN (VCard v) =
      case length fn of
        0 -> VCard $ v { fn = gen }
        _ -> VCard $ v { fn = gen, props = notFN }
      where
      fn = filter (\(Property e) -> e.name == "FN") v.props
      notFN = filter (\(Property e) -> e.name /= "FN") v.props
      getParams (Property p) = p.params
      gen = case toOther v.name of
        "" -> Property { name: "FN", value: Value "unknown", group: "", params: [] }
        _ -> Property { name: "FN", value: Value (toOther v.name), group: "", params: getParams v.name }

    -- transforms Properties with the ADR name to Addresses
    transformAdrs :: VCard -> VCard
    transformAdrs (VCard v) =
      VCard $ v { adrs = addresses, props = notAdr }
      where
      adr = filter (\(Property e) -> e.name == "ADR") v.props
      notAdr = filter (\(Property e) -> e.name /= "ADR") v.props
      addresses = map (\(Property e) -> Property { name: e.name, value: toOther e.value, group: e.group, params: e.params }) adr

    -- unescapes values in a vcard
    unescapeVCard :: VCard -> VCard
    unescapeVCard (VCard v) =
      VCard $ v { props = flip map v.props \(Property p) -> Property $ p { value = unEscapeValue p.value } }
