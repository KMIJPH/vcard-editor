-- File Name: Common.purs
-- Description: Common VCard Types and Classes
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 02 May 2023 15:33:31
-- Last Modified: 23 Nov 2023 15:19:05

module VCF.Type.Common where

import Prelude

import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype)
import Data.String (joinWith)
import Parsing (runParser)
import VCF.Encoding (Encodings(..), _decQP, _encQP)
import VCF.Parsers (escapeAll, indentNewlines, splitLines, unEscapeAll)

-- | empty type
class Empty a where
  empty :: a

-- | creates type from a more primitive type
class FromPrimitive a b where
  fromPrimitive :: Encodings -> a -> b

-- | writes to string
class ToString a where
  toString :: Maybe Encodings -> a -> String

-- | from one type to another
class ToOther a b where
  toOther :: a -> b

newtype Value = Value String

derive instance eqValue :: Eq Value
derive instance newtypeValue :: Newtype Value _
instance showValue :: Show Value where
  show (Value v) = show v

instance emptyValue :: Empty Value where
  empty = Value ""

-- Unfolding is done before any decoding of a type value in a content line.
instance fromPrimitiveValue :: FromPrimitive Value Value where
  fromPrimitive encoding (Value string) =
    case encoding of
      QP -> Value $ _decQP string -- do not unindent QP
      EightBit -> Value string -- do not unindent EightBit
      _ -> Value unwrapped -- do not decode binary
    where
    unwrapped = case runParser string splitLines of
      Right lines -> joinWith "" lines
      Left _ -> string

-- Folding is done after any content encoding of a type value.
instance toStringValue :: ToString Value where
  toString encoding (Value v) = do
    case encoding of
      Just QP -> _encQP indented
      Just Base64 -> indented <> "\r\n" -- add a empty line (v2.1)
      _ -> indented -- do not encode
    where
    indented = case runParser v indentNewlines of
      Right replaced -> replaced
      Left _ -> v

instance toOtherValueString :: ToOther Value String where
  toOther (Value v) = v

newtype Parameter = Parameter { param :: String, value :: String }

derive instance eqParam :: Eq Parameter
derive instance newtypeParam :: Newtype Parameter _
instance showParam :: Show Parameter where
  show (Parameter p) = show p

instance emptyParam :: Empty Parameter where
  empty = Parameter { param: "", value: "" }

-- parameter should not need decoding
instance fromPrimitiveParameter :: FromPrimitive Parameter Parameter where
  fromPrimitive _ (Parameter p) =
    Parameter { param: p.param, value: unEscapeAll p.value }

-- parameter should not need encoding
instance toStringParameter :: ToString Parameter where
  toString _ (Parameter p) =
    case p.param of
      "" -> escapeAll p.value
      _ -> p.param <> "=" <> escapeAll p.value -- parameter name should not need escaping

escapeValue :: Value -> Value
escapeValue (Value v) = Value $ escapeAll v

unEscapeValue :: Value -> Value
unEscapeValue (Value v) = Value $ unEscapeAll v

