-- File Name: Property.purs
-- Description: VCard Property
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 02 May 2023 15:05:04
-- Last Modified: 23 Nov 2023 15:25:12

module VCF.Type.Property where

import Prelude

import Data.Array (length, concat, elem, filter)
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype)
import Data.String (joinWith)
import VCF.Encoding (Encodings(..))
import VCF.Parsers (isValidAscii)
import VCF.Type.Common
  ( class Empty
  , class FromPrimitive
  , class ToOther
  , class ToString
  , Parameter(..)
  , Value
  , empty
  , fromPrimitive
  , toOther
  , toString
  )
import VCF.Type.Name (Name)

newtype Property a = Property { name :: String, value :: a, group :: String, params :: Array Parameter }

derive instance eqProperty :: Eq a => Eq (Property a)
derive instance newtypeProperty :: Newtype (Property a) _
instance showProperty :: Show a => Show (Property a) where
  show (Property p) = show p

instance ordProperty :: Eq a => Ord (Property a) where
  compare (Property p1) (Property p2) =
    if p1 == p2 then EQ
    else if p1.name > p2.name then GT
    else LT

instance emptyProperty :: Empty a => Empty (Property a) where
  empty = Property { name: "", value: empty, group: "", params: [] }

instance fromPrimitiveProperty :: (FromPrimitive Value a) => FromPrimitive (Property Value) (Property a) where
  fromPrimitive _ (Property p) =
    Property $ p { value = value, params = map (\pp -> toParam pp) params }
    where
    toParam :: Parameter -> Parameter
    toParam param = fromPrimitive QP param

    encoding = encodingInParams (Property p) Nothing

    value = case encoding of
      Just enc -> fromPrimitive enc p.value
      Nothing -> fromPrimitive EightBit p.value

    params = case encoding of
      Just QP -> filter (\(Parameter pp) -> pp.param /= "ENCODING") p.params
      _ -> p.params

instance toStringProperty :: (ToString a, Eq a, Show a, Empty a) => ToString (Property a) where
  toString default (Property p) =
    case Property p == empty of
      true -> ""
      false ->
        -- group should not need escape
        p.group <> (if p.group == "" then "" else ".")
          <> p.name -- name should not need escape
          <> case length params of
            0 -> ":"
            _ -> ";" <> (joinWith ";" (map (\e -> toString default e) params)) <> ":"
          <> value
    where
    encoding = encodingInParams (Property p) default
    value = toString encoding p.value
    params = case encoding of
      Just enc -> concat [ filter (\(Parameter pp) -> pp.param /= "ENCODING") p.params, [ encoder enc ] ]
      Nothing -> p.params

instance toOtherPropertyNameString :: ToOther (Property Name) String where
  toOther (Property p) =
    toOther p.value

instance toOtherPropertyValueGeneric :: ToOther Value a => ToOther (Property Value) a where
  toOther (Property p) =
    toOther p.value

encoder :: Encodings -> Parameter
encoder e =
  case e of
    QP -> Parameter { param: "ENCODING", value: "QUOTED-PRINTABLE" }
    Base64 -> Parameter { param: "ENCODING", value: "BASE64" }
    EightBit -> Parameter { param: "ENCODING", value: "8BIT" }
    Binary -> Parameter { param: "ENCODING", value: "B" } -- version 3.0

encodingInParams :: forall a. Show a => Property a -> Maybe Encodings -> Maybe Encodings
encodingInParams (Property p) default =
  case elem (encoder QP) p.params of
    true -> Just QP
    false -> case elem (encoder Base64) p.params of
      true -> Just Base64
      false -> case elem (encoder EightBit) p.params of
        true -> Just EightBit
        false -> case elem (encoder Binary) p.params || elem (Parameter { param: "ENCODING", value: "b" }) p.params of
          true -> Just Binary
          false -> case isValidAscii (show p.value) of
            true -> Nothing
            false -> default
