-- File Name: Name.purs
-- Description: VCard Name
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 02 May 2023 15:05:04
-- Last Modified: 08 Nov 2023 12:37:06

module VCF.Type.Name where

import Prelude

import Data.Array (toUnfoldable, filter)
import Data.Either (Either(..))
import Data.List (List(Nil), (:))
import Data.Newtype (class Newtype)
import Data.String (joinWith)
import Parsing (runParser)
import VCF.Parsers (escapeAll, splitPart, unEscapeAll)
import VCF.Type.Common
  ( class Empty
  , class FromPrimitive
  , class ToOther
  , class ToString
  , Value(..)
  , empty
  , toOther
  , toString
  , fromPrimitive
  )

newtype Name = Name
  { family :: String
  , given :: String
  , add :: String
  , prefix :: String
  , suffix :: String
  }

-- https://en.wikipedia.org/wiki/Personal_name#Name_order
data NameOrder = Western | Eastern

derive instance eqName :: Eq Name
derive instance newtypeName :: Newtype Name _

instance showName :: Show Name where
  show (Name n) = show n

instance emptyName :: Empty Name where
  empty = Name { family: "", given: "", add: "", prefix: "", suffix: "" }

instance fromPrimitiveName :: FromPrimitive Value Name where
  fromPrimitive encoding value =
    Name
      { family: unEscapeAll n.family
      , given: unEscapeAll n.given
      , add: unEscapeAll n.add
      , prefix: unEscapeAll n.prefix
      , suffix: unEscapeAll n.suffix
      }
    where
    fp :: Value -> Value
    fp v = fromPrimitive encoding v

    name = toOther $ fp value -- this value should be decoded
    fromName (Name na) = na
    n = fromName name

instance toStringName :: ToString Name where
  toString encoding name =
    toString encoding (toValue name) -- should take care of encoding etc.
    where
    toValue :: Name -> Value
    toValue n = toOther n -- escapes the value

instance toOtherNameValue :: ToOther Name Value where
  toOther (Name n) =
    case Name n == empty of
      true -> Value ""
      false ->
        Value
          $ escapeAll n.family
              <> ";"
              <> escapeAll n.given
              <> ";"
              <> escapeAll n.add
              <> ";"
              <> escapeAll n.prefix
              <> ";"
              <> escapeAll n.suffix

instance toOtherValueName :: ToOther Value Name where
  toOther a =
    case runParser (toOther a) splitPart of
      Left _ -> empty
      Right parts ->
        case toUnfoldable parts of
          (p1 : Nil) -> Name { family: p1, given: "", add: "", prefix: "", suffix: "" }
          (p1 : p2 : Nil) -> Name { family: p1, given: p2, add: "", prefix: "", suffix: "" }
          (p1 : p2 : p3 : Nil) -> Name { family: p1, given: p2, add: p3, prefix: "", suffix: "" }
          (p1 : p2 : p3 : p4 : Nil) -> Name { family: p1, given: p2, add: p3, prefix: p4, suffix: "" }
          (p1 : p2 : p3 : p4 : p5 : _) -> Name { family: p1, given: p2, add: p3, prefix: p4, suffix: p5 }
          _ -> empty

-- | creates FN
instance toOtherNameString :: ToOther Name String where
  toOther (Name n) =
    joinWith " " $ filter (\e -> e /= "") arr
    where
    arr = [ n.prefix, n.given, n.add, n.family, n.suffix ]
