"use strict";

// decodes quoted printable string
export function _decQP(string) {
    const encoded = string.replace(/={1}/g, "%");
    try {
        return decodeURI(encoded);
    } catch {
        return string;
    }
}

// encodes to quoted printable
export function _encQP(string) {
    try {
        var encoded = encodeURI(string)
        return encoded.replace(/%{1}/g, "=");
    } catch {
        return string;
    }
}

// decodes base64 string
export function _decBase64(string) {
    try {
        return atob(string);
    } catch {
        return string;
    }
}

// encodes to base64
export function _encBase64(string) {
    try {
        return btoa(string);
    } catch {
        return string;
    }
}
