-- File Name: Main.purs
-- Description: vCard editor
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 29 Apr 2023 15:44:17
-- Last Modified: 14 Nov 2023 21:47:37

module Main where

import Prelude

import Data.Array (catMaybes, fromFoldable, index, length, updateAt)
import Data.Const (Const)
import Data.Either (Either(..))
import Data.Foldable (traverse_)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Aff.Class (class MonadAff)
import Effect.Class.Console (log)
import Halogen as H
import Halogen.Aff (awaitLoad, runHalogenAff, selectElement)
import Halogen.HTML as HH
import Halogen.VDom.Driver (runUI)
import Parsing (runParser)
import Parsing.Combinators (many1)
import Type.Proxy (Proxy(..))
import UI.EditView as ChildEdit
import UI.FileDownload (download)
import UI.FileView as ChildFile
import UI.ListView as ChildList
import UI.Components.SettingsButton as ChildSettings
import UI.Style as Style
import UI.Util (getScroll, timeStamp)
import VCF.Reader (nested, single)
import VCF.Type.VCard (VCard)
import VCF.Writer (ExportSettings, defaultSettings, writeVCards)
import Web.DOM.ParentNode (QuerySelector(..))

-- TODO: maybe add routing
-- TODO: clean up some UI code

data CurrentView
  = Home
  | ListView (Array VCard)
  | EditView (Tuple VCard Int)

data Action
  = DoNothing
  | ExportItems (Array VCard)
  | GotoEdit (Tuple (Maybe VCard) Int)
  | GotoHome
  | GotoList
  | InitializeList (Tuple String String)
  | StoreItems (Array VCard)
  | StoreFilter ChildSettings.Filter
  | StoreSelection (Array Int)
  | StoreSettings (ExportSettings)
  | UpdateVCard (Tuple VCard Int)

type Slots =
  ( file :: H.Slot (ChildFile.Query) ChildFile.Message Int
  , list :: H.Slot (Const Void) ChildList.Message Int
  , edit :: H.Slot (Const Void) ChildEdit.Message Int
  )

type State =
  { currentView :: CurrentView
  , filename :: String
  , filter :: ChildSettings.Filter
  , scroll :: Number
  , selection :: Array Int
  , settings :: ExportSettings
  , store :: Array VCard
  }

_file = Proxy :: Proxy "file"
_list = Proxy :: Proxy "list"
_edit = Proxy :: Proxy "edit"

initialState :: forall i. i -> State
initialState _ =
  { currentView: Home
  , filename: ""
  , filter: ChildSettings.Reset
  , scroll: 0.0
  , selection: []
  , settings: defaultSettings
  , store: []
  }

component :: forall q i o m. MonadAff m => H.Component q i o m
component =
  H.mkComponent
    { initialState
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction }
    }

render :: forall m. MonadAff m => State -> H.ComponentHTML Action Slots m
render state =
  case state.currentView of
    Home -> HH.slot _file 0 (ChildFile.component) unit listenFile
    ListView items ->
      HH.slot
        _list
        0
        (ChildList.component)
        { items: items, selection: state.selection, scroll: state.scroll, settings: state.settings, filter: state.filter }
        listenList
    EditView (Tuple vcard i) -> HH.slot _edit 0 (ChildEdit.component) { vcard, index: i } listenEdit

handleAction :: forall o m. MonadAff m => Action -> H.HalogenM State Action Slots o m Unit
handleAction action = case action of
  DoNothing -> pure unit
  ExportItems items -> do
    state <- H.get
    let selected = catMaybes $ map (\i -> index items i) state.selection
    let written = writeVCards selected state.settings
    ts <- H.liftEffect timeStamp
    _ <- H.liftEffect $ download written (state.filename <> "-" <> ts <> ".vcf") "text/vcard"
    pure unit
  GotoEdit (Tuple vcard i) -> case vcard of
    Just v -> do
      scroll <- H.liftEffect getScroll
      H.modify_ \s -> s { currentView = EditView $ Tuple v i, scroll = scroll }
    Nothing -> log "No VCard to edit provided"
  GotoHome -> H.modify_ \s -> s { currentView = Home }
  GotoList -> H.modify_ \s -> s { currentView = ListView s.store }
  InitializeList (Tuple content fn) -> do
    H.modify_ \s -> s { filename = fn }
    parseV2 content
  StoreItems items -> H.modify_ \s -> s { store = items }
  StoreFilter filter -> H.modify_ \s -> s { filter = filter }
  StoreSelection sel -> H.modify_ \s -> s { selection = sel }
  StoreSettings settings -> H.modify_ \s -> s { settings = settings }
  UpdateVCard (Tuple vcard i) -> H.modify_ \s -> s { store = fromMaybe s.store $ updateAt i vcard s.store }

listenFile :: ChildFile.Message -> Action
listenFile message = case message of
  ChildFile.Content (Tuple content filename) -> InitializeList $ Tuple content filename
  ChildFile.NoContent -> DoNothing

listenList :: ChildList.Message -> Action
listenList message = case message of
  ChildList.ExportItems items -> ExportItems items
  ChildList.GotoEdit (Tuple vcard i) -> GotoEdit $ Tuple vcard i
  ChildList.GotoHome -> GotoHome
  ChildList.SendItems items -> StoreItems items
  ChildList.SendSelection selection -> StoreSelection selection
  ChildList.SendSettings settings -> StoreSettings settings
  ChildList.SendFilter filter -> StoreFilter filter

listenEdit :: ChildEdit.Message -> Action
listenEdit message = case message of
  ChildEdit.GotoList -> GotoList
  ChildEdit.UpdateVCard (Tuple vcard i) -> UpdateVCard $ Tuple vcard i

parseV2 :: forall o m. MonadAff m => String -> H.HalogenM State Action Slots o m Unit
parseV2 input = do
  case runParser input nested of
    Right items -> do
      case length items of
        0 -> runSingle
        _ -> H.modify_ \s -> s { currentView = ListView $ fromFoldable $ items }
    Left err -> do
      H.tell _file 0 (ChildFile.SendError (show err))
      runSingle
  where
  runSingle :: forall oo mm. MonadAff mm => H.HalogenM State Action Slots oo mm Unit
  runSingle =
    case runParser input (many1 (single true)) of
      Right items -> do
        H.modify_ \s -> s { currentView = ListView $ fromFoldable $ items }
      Left err -> do
        H.tell _file 0 (ChildFile.SendError (show err))

main :: Effect Unit
main = runHalogenAff do
  awaitLoad
  traverse_ (runUI Style.component unit) =<< selectElement (QuerySelector "head")
  traverse_ (runUI component unit) =<< selectElement (QuerySelector ".vdom-content")
