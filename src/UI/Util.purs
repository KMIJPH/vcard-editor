-- File Name: Util.purs
-- Description: VDOM stuff
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 30 Okt 2023 22:49:51
-- Last Modified: 13 Nov 2023 08:27:34

module UI.Util where

import Prelude

import Data.Array (replicate)
import Data.DateTime as DT
import Data.Enum (fromEnum)
import Data.Int (ceil)
import Data.Maybe (Maybe(..))
import Data.String (length)
import Data.String.CodeUnits (fromCharArray)
import Effect (Effect)
import Effect.Now as Now
import Web.DOM.ChildNode (remove)
import Web.DOM.Element as Elem
import Web.DOM.ParentNode (QuerySelector(..), querySelector)
import Web.HTML as HTML
import Web.HTML.HTMLDocument as Doc
import Web.HTML.HTMLElement as HTMLElem
import Web.HTML.Window as Window

getScroll :: Effect Number
getScroll = do
  w <- HTML.window
  Window.scrollY w

setScroll :: Number -> Effect Unit
setScroll scrollY = do
  w <- HTML.window
  Window.scroll 0 (ceil scrollY) w

timeStamp :: Effect String
timeStamp = do
  datetime <- Now.nowDateTime
  let date = DT.date datetime
  let time = DT.time datetime
  let year = padZeros 2 $ fromEnum $ DT.year date
  let month = padZeros 2 $ fromEnum $ DT.month date
  let day = padZeros 2 $ fromEnum $ DT.day date
  let hour = padZeros 2 $ fromEnum $ DT.hour time
  let minute = padZeros 2 $ fromEnum $ DT.minute time
  let second = padZeros 2 $ fromEnum $ DT.second time
  pure $ (year <> month <> day <> "-" <> hour <> minute <> second)
  where
  -- | Pad a string with the given character up to a maximum length.
  padLeft :: Char -> Int -> String -> String
  padLeft c len str = prefix <> str
    where
    prefix = fromCharArray (replicate (len - length str) c)

  -- | Pad a number with leading zeros up to the given length.
  padZeros :: Int -> Int -> String
  padZeros len num
    | num >= 0 = padLeft '0' len (show num)
    | otherwise = "-" <> padLeft '0' len (show (-num))

removeElement :: String -> Effect Unit
removeElement elem = do
  w <- HTML.window
  d <- Window.document w
  maybeBody <- Doc.body d
  case maybeBody of
    Just body -> do
      sel <- querySelector (QuerySelector elem) (HTMLElem.toParentNode body)
      case sel of
        Just e -> remove (Elem.toChildNode e)
        Nothing -> pure unit
      pure unit
    Nothing -> pure unit
