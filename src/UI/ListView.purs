-- File Name: ListView.purs
-- Description: Main list view
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 21 Okt 2023 18:57:03
-- Last Modified: 23 Nov 2023 11:13:07

module UI.ListView where

import Prelude

import Data.Array (any, catMaybes, cons, elem, filter, filterA, index, length, mapWithIndex, range, reverse, sort)
import Data.Const (Const)
import Data.Foldable (for_)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.String (Pattern(..), contains, toLower, toUpper)
import Data.Tuple (Tuple(..))
import Effect.Aff.Class (class MonadAff)
import Effect.Class (class MonadEffect)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Type.Proxy (Proxy(..))
import UI.Components.HelpButton as ChildHelp
import UI.Components.ListItem as ChildLI
import UI.Components.SettingsButton as ChildSettings
import UI.Style (classes)
import UI.Util (setScroll)
import VCF.Type.Common (Value, empty, toOther)
import VCF.Type.Property (Property(..))
import VCF.Type.VCard (VCard(..))
import VCF.Writer (ExportSettings)

data Action
  = Add
  | Edit Int
  | Export
  | Finalize
  | GoBack
  | Initialize
  | ReceiveFilter ChildSettings.Filter
  | ReceiveSettings ExportSettings
  | RemoveSelected
  | Sort
  | ToggleSelect

data Message
  = GotoHome
  | ExportItems (Array VCard)
  | GotoEdit (Tuple (Maybe VCard) Int)
  | SendFilter ChildSettings.Filter
  | SendItems (Array VCard)
  | SendSelection (Array Int)
  | SendSettings ExportSettings

type ListInput =
  { items :: Array VCard
  , filter :: ChildSettings.Filter
  , scroll :: Number
  , selection :: Array Int
  , settings :: ExportSettings
  }

type State =
  { items :: Array VCard
  , filter :: ChildSettings.Filter
  , selected :: Boolean
  , selection :: Array Int
  , settings :: ExportSettings
  , scroll :: Number
  , sort :: Boolean
  }

type Slots =
  ( help :: H.Slot (Const Void) Unit Int
  , item :: H.Slot ChildLI.Query ChildLI.Message Int
  , settings :: H.Slot (Const Void) ChildSettings.Message Int
  )

_item = Proxy :: Proxy "item"
_help = Proxy :: Proxy "help"
_settings = Proxy :: Proxy "settings"

initialState :: ListInput -> State
initialState { items, scroll, selection, settings, filter } =
  { items
  , filter
  , selection
  , scroll
  , settings
  , selected: false
  , sort: false
  }

component :: forall q m. MonadAff m => H.Component q ListInput Message m
component =
  H.mkComponent
    { initialState: initialState
    , render
    , eval: H.mkEval $ H.defaultEval
        { handleAction = handleAction
        , initialize = Just Initialize -- create
        , finalize = Just Finalize -- delete
        }
    }

render :: forall m. MonadEffect m => State -> H.ComponentHTML Action Slots m
render state =
  HH.div_
    [ HH.div [ HP.class_ (HH.ClassName classes.navigation) ]
        [ HH.div [ HP.class_ (HH.ClassName classes.navigationLeft) ]
            [ HH.button [ HP.class_ $ HH.ClassName classes.buttonBack, HP.title "back", HE.onClick \_ -> GoBack ] []
            , HH.button [ HP.class_ $ HH.ClassName classes.buttonSelect, HP.title "select all", HE.onClick \_ -> ToggleSelect ] []
            , HH.button [ HP.class_ $ HH.ClassName classes.buttonAdd, HP.title "add entry", HE.onClick \_ -> Add ] []
            , HH.button [ HP.class_ $ HH.ClassName classes.buttonRemove, HP.title "remove selected", HE.onClick \_ -> RemoveSelected ] []
            , HH.button [ HP.class_ $ HH.ClassName classes.buttonSort, HP.title "sort", HE.onClick \_ -> Sort ] []
            , HH.button [ HP.class_ $ HH.ClassName classes.buttonSave, HP.title "export selected", HE.onClick \_ -> Export ] []
            ]
        , HH.div [ HP.class_ (HH.ClassName classes.navigationRight) ]
            [ HH.slot_ _help 0 (ChildHelp.component) unit
            , HH.slot _settings 0 (ChildSettings.component) { isOpen: false, settings: state.settings } listenSettings
            ]
        ]
    , HH.p [ HP.class_ $ HH.ClassName classes.dim ] [ HH.text ((show $ length (applyFilter state.items state.filter)) <> " entries found") ]
    , HH.div [ HP.class_ $ HH.ClassName classes.list ] $ flip mapWithIndex items \i (VCard v) ->
        HH.slot _item i (ChildLI.component) { label: (toOther v.name), index: i } listenLI
    ]
  where
  items = flip map (applyFilter state.items state.filter) \(Tuple item _) -> item

listenLI :: ChildLI.Message -> Action
listenLI message = case message of
  ChildLI.GotoEdit i -> Edit i

listenSettings :: ChildSettings.Message -> Action
listenSettings message = case message of
  ChildSettings.SendSettings settings -> ReceiveSettings settings
  ChildSettings.SendFilter f -> ReceiveFilter f

handleAction :: forall m. MonadAff m => Action -> H.HalogenM State Action Slots Message m Unit
handleAction action = case action of
  Add -> do
    state <- H.modify \s -> s { items = cons empty s.items }
    H.raise (SendItems state.items)
    H.raise (GotoEdit $ Tuple (index state.items 0) 0)
  Edit i -> do
    state <- H.get
    let items = applyFilter state.items state.filter
    case index items i of
      Just (Tuple item index) -> H.raise (GotoEdit $ Tuple (Just item) index)
      Nothing -> pure unit
  Export -> do
    state <- H.get
    indices <- getSelected state true
    let items = applyFilter state.items state.filter
    let filtered = catMaybes $ map (\i -> index items i) indices
    H.raise (SendSelection $ catMaybes $ realIndices state.items (map (\(Tuple v _) -> v) filtered))
    H.raise $ ExportItems state.items
    where
    -- compares items with filtered items and returns the indices
    realIndices :: Array VCard -> Array VCard -> Array (Maybe Int)
    realIndices items items' =
      flip mapWithIndex items \i v ->
        case elem v items' of
          true -> Just i
          false -> Nothing
  Finalize -> do
    state <- H.get
    indices <- getSelected state true
    H.raise (SendSelection indices)
    H.raise (SendItems state.items)
  GoBack -> H.raise GotoHome
  Initialize -> do
    state <- H.get
    H.liftEffect $ setScroll state.scroll
    for_ state.selection \i ->
      H.tell _item i (ChildLI.SelectItem true)
  RemoveSelected -> do
    state <- H.get
    indices <- getSelected state true
    let items = applyFilter state.items state.filter
    let filtered = catMaybes $ map (\i -> index items i) indices
    let indices' = map (\(Tuple _ i) -> i) filtered
    let notSelected = filter (\i -> not $ elem i indices') (range 0 ((length state.items) - 1))
    H.modify_ \s -> s { items = catMaybes $ map (\i -> index state.items i) notSelected }
  ReceiveFilter f -> do
    H.modify_ \s -> s { filter = f }
    H.raise $ SendFilter f
  ReceiveSettings settings -> H.raise (SendSettings settings)
  Sort -> do
    state <- H.get
    case state.sort of
      false -> H.modify_ \s -> s { items = sort s.items, selection = [], sort = true }
      true -> H.modify_ \s -> s { items = reverse $ sort s.items, selection = [], sort = false }
  ToggleSelect -> do
    state <- H.modify \s -> s { selected = not s.selected }
    for_ (range 0 ((length state.items) - 1)) \i ->
      H.tell _item i (ChildLI.SelectItem state.selected)

-- | returns indices of selected/not-selected entries
getSelected :: forall m. MonadAff m => State -> Boolean -> H.HalogenM State Action Slots Message m (Array Int)
getSelected state b = do
  filterA
    ( \i -> do
        selected <- H.request _item i ChildLI.IsSelected
        case b of
          true -> pure $ fromMaybe false selected
          false -> pure $ not $ fromMaybe true selected
    )
    (range 0 ((length state.items) - 1))

-- | applies a filter to the items
applyFilter :: Array VCard -> ChildSettings.Filter -> Array (Tuple VCard Int)
applyFilter items f =
  case f of
    ChildSettings.Name name -> filterWithIndex items (filterFN name)
    ChildSettings.Org value -> filterWithIndex items (filterOrg value)
    ChildSettings.Reset -> flip mapWithIndex items \i v -> Tuple v i
  where
  -- filter items and returns items with (real) indices
  filterWithIndex :: Array VCard -> (VCard -> Boolean) -> Array (Tuple VCard Int)
  filterWithIndex items' func =
    catMaybes indices
    where
    indices = flip mapWithIndex items' \i v ->
      case func v of
        true -> Just $ Tuple v i
        false -> Nothing

  -- checks pattern, lowercase pattern and uppercase pattern
  containsPattern :: String -> String -> Boolean
  containsPattern pattern value =
    contains (Pattern pattern) value
      || contains (Pattern pattern) (toLower value)
      || contains (Pattern pattern) (toUpper value)

  getValue :: Property Value -> String
  getValue (Property p) = toOther p.value

  -- filters based on FN
  filterFN :: String -> VCard -> Boolean
  filterFN pattern (VCard v) =
    containsPattern pattern (getValue v.fn)

  -- filters based on ORG
  filterOrg :: String -> VCard -> Boolean
  filterOrg pattern (VCard v) =
    any (\c -> c == true) matches
    where
    orgs = filter (\(Property p) -> p.name == "ORG") v.props
    matches = map (\p -> containsPattern pattern (getValue p)) orgs
