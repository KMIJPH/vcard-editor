-- File Name: Style.purs
-- Description: CSS style stuff
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 21 Okt 2023 18:57:46
-- Last Modified: 24 Nov 2023 22:32:09

module UI.Style where

import Prelude

import CSS ((|*), (|>))
import CSS as C
import CSS.Color as Col
import CSS.Common as Common
import CSS.Cursor as Cursor
import CSS.Flexbox as Flexbox
import CSS.Overflow as Overflow
import CSS.Selector as CS
import CSS.TextAlign as CT
import CSS.VerticalAlign as VA
import Data.String (Pattern(..), Replacement(..), replace)
import Halogen as H
import Halogen.HTML.CSS as HS

type Ids =
  { nameSuggestions :: String
  , valueSuggestions :: String
  , propSuggestions :: String
  , filterInput :: String
  }

ids :: Ids
ids =
  { nameSuggestions: "vcard-name-sug"
  , valueSuggestions: "vcard-value-sug"
  , propSuggestions: "vcard-prop-sug"
  , filterInput: "vcard-filter-input"
  }

type Classes =
  { boxed :: String -- boxed divs in edit view
  , buttonAdd :: String
  , buttonAddAddr :: String
  , buttonAddProp :: String
  , buttonBack :: String
  , buttonEdit :: String
  , buttonExtraSmall :: String
  , buttonHelp :: String
  , buttonSettings :: String
  , buttonRemove :: String
  , buttonSave :: String
  , buttonSelect :: String -- select all (list view)
  , buttonSmall :: String
  , buttonSort :: String
  , dim :: String
  , dropdown :: String -- dropdown menus
  , dropdownContent :: String -- the stuff inside dropdowns
  , dropdownSettings :: String
  , fieldGroup :: String -- the group field (in addresses and properties)
  , fieldInvalid :: String -- field invalid after validity check
  , fieldName :: String -- the field name (left form)
  , fileSelector :: String -- upload file selector
  , fileView :: String -- file view div
  , form :: String -- name, address, property forms
  , formField :: String -- single form field (name, value)
  , formName :: String -- form name (e.g. Address, Property..)
  , formParam :: String -- parameter container
  , list :: String -- list view container
  , listCheckBox :: String -- the checkbox in a list item
  , listItem :: String -- a list item
  , navigation :: String -- navigation section
  , navigationLeft :: String
  , navigationRight :: String
  , paramTitle :: String -- parameter title
  }

classes :: Classes
classes =
  { boxed: "vcard-boxed"
  , buttonAdd: "vcard-button-add"
  , buttonAddAddr: "vcard-button-add-addr"
  , buttonAddProp: "vcard-button-add-prop"
  , buttonBack: "vcard-button-back"
  , buttonEdit: "vcard-button-edit"
  , buttonExtraSmall: "vcard-button-extra-small"
  , buttonSettings: "vcard-button-settings"
  , buttonHelp: "vcard-button-help"
  , buttonRemove: "vcard-button-remove"
  , buttonSave: "vcard-button-save"
  , buttonSelect: "vcard-button-select"
  , buttonSmall: "vcard-button-small"
  , buttonSort: "vcard-button-sort"
  , dim: "vcard-dim"
  , dropdown: "vcard-dropdown"
  , dropdownContent: "vcard-dropdown-content"
  , dropdownSettings: "vcard-dropdown-settings"
  , fieldGroup: "vcard-field-group"
  , fieldInvalid: "vcard-field-invalid"
  , fieldName: "vcard-field-name"
  , fileSelector: "vcard-file-selector"
  , fileView: "vcard-file-view"
  , form: "vcard-form"
  , formField: "vcard-form-field"
  , formName: "vcard-form-name"
  , formParam: "vcard-form-param"
  , list: "vcard-list"
  , listCheckBox: "vcard-list-checkbox"
  , listItem: "vcard-list-item"
  , navigation: "vcard-navigation"
  , navigationLeft: "vcard-navigation-left"
  , navigationRight: "vcard-navigation-right"
  , paramTitle: "vcard-param-title"
  }

type Colors =
  { bg :: Col.Color
  , hover :: Col.Color
  , invalid :: Col.Color
  , transparent :: Col.Color
  }

colors :: Colors
colors =
  { bg: Col.rgb 30 90 100
  , hover: Col.rgb 80 160 170
  , invalid: Col.rgba 255 0 0 0.3
  , transparent: Col.rgba 0 0 0 0.0
  }

type SVG =
  { path :: String
  , viewbox :: String
  }

type Buttons =
  { add :: SVG
  , address :: SVG
  , back :: SVG
  , edit :: SVG
  , help :: SVG
  , property :: SVG
  , remove :: SVG
  , save :: SVG
  , select :: SVG
  , settings :: SVG
  , sort :: SVG
  }

svgPaths :: Buttons
svgPaths =
  { add: { path: "M256 80c0-17.7-14.3-32-32-32s-32 14.3-32 32V224H48c-17.7 0-32 14.3-32 32s14.3 32 32 32H192V432c0 17.7 14.3 32 32 32s32-14.3 32-32V288H400c17.7 0 32-14.3 32-32s-14.3-32-32-32H256V80z", viewbox: "0 0 448 512" }
  , address: { path: "M64 32C28.7 32 0 60.7 0 96V416c0 35.3 28.7 64 64 64H512c35.3 0 64-28.7 64-64V96c0-35.3-28.7-64-64-64H64zm80 256h64c44.2 0 80 35.8 80 80c0 8.8-7.2 16-16 16H80c-8.8 0-16-7.2-16-16c0-44.2 35.8-80 80-80zm-32-96a64 64 0 1 1 128 0 64 64 0 1 1 -128 0zm256-32H496c8.8 0 16 7.2 16 16s-7.2 16-16 16H368c-8.8 0-16-7.2-16-16s7.2-16 16-16zm0 64H496c8.8 0 16 7.2 16 16s-7.2 16-16 16H368c-8.8 0-16-7.2-16-16s7.2-16 16-16zm0 64H496c8.8 0 16 7.2 16 16s-7.2 16-16 16H368c-8.8 0-16-7.2-16-16s7.2-16 16-16z", viewbox: "0 0 576 512" }
  , back: { path: "M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l128 128c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.3 288 480 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-370.7 0 73.4-73.4c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-128 128z", viewbox: "0 0 512 512" }
  , edit: { path: "M410.3 231l11.3-11.3-33.9-33.9-62.1-62.1L291.7 89.8l-11.3 11.3-22.6 22.6L58.6 322.9c-10.4 10.4-18 23.3-22.2 37.4L1 480.7c-2.5 8.4-.2 17.5 6.1 23.7s15.3 8.5 23.7 6.1l120.3-35.4c14.1-4.2 27-11.8 37.4-22.2L387.7 253.7 410.3 231zM160 399.4l-9.1 22.7c-4 3.1-8.5 5.4-13.3 6.9L59.4 452l23-78.1c1.4-4.9 3.8-9.4 6.9-13.3l22.7-9.1v32c0 8.8 7.2 16 16 16h32zM362.7 18.7L348.3 33.2 325.7 55.8 314.3 67.1l33.9 33.9 62.1 62.1 33.9 33.9 11.3-11.3 22.6-22.6 14.5-14.5c25-25 25-65.5 0-90.5L453.3 18.7c-25-25-65.5-25-90.5 0zm-47.4 168l-144 144c-6.2 6.2-16.4 6.2-22.6 0s-6.2-16.4 0-22.6l144-144c6.2-6.2 16.4-6.2 22.6 0s6.2 16.4 0 22.6z", viewbox: "0 0 512 512" }
  , help: { path: "M80 160c0-35.3 28.7-64 64-64h32c35.3 0 64 28.7 64 64v3.6c0 21.8-11.1 42.1-29.4 53.8l-42.2 27.1c-25.2 16.2-40.4 44.1-40.4 74V320c0 17.7 14.3 32 32 32s32-14.3 32-32v-1.4c0-8.2 4.2-15.8 11-20.2l42.2-27.1c36.6-23.6 58.8-64.1 58.8-107.7V160c0-70.7-57.3-128-128-128H144C73.3 32 16 89.3 16 160c0 17.7 14.3 32 32 32s32-14.3 32-32zm80 320a40 40 0 1 0 0-80 40 40 0 1 0 0 80z", viewbox: "0 0 320 512" }
  , property: { path: "M256 80c0-17.7-14.3-32-32-32s-32 14.3-32 32V224H48c-17.7 0-32 14.3-32 32s14.3 32 32 32H192V432c0 17.7 14.3 32 32 32s32-14.3 32-32V288H400c17.7 0 32-14.3 32-32s-14.3-32-32-32H256V80z", viewbox: "0 0 512 512" }
  , remove: { path: "M342.6 150.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L192 210.7 86.6 105.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L146.7 256 41.4 361.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L192 301.3 297.4 406.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L237.3 256 342.6 150.6z", viewbox: "0 0 384 512" }
  , save: { path: "M64 32C28.7 32 0 60.7 0 96V416c0 35.3 28.7 64 64 64H384c35.3 0 64-28.7 64-64V173.3c0-17-6.7-33.3-18.7-45.3L352 50.7C340 38.7 323.7 32 306.7 32H64zm0 96c0-17.7 14.3-32 32-32H288c17.7 0 32 14.3 32 32v64c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V128zM224 288a64 64 0 1 1 0 128 64 64 0 1 1 0-128z", viewbox: "0 0 512 512" }
  , select: { path: "M64 32C28.7 32 0 60.7 0 96V416c0 35.3 28.7 64 64 64H384c35.3 0 64-28.7 64-64V96c0-35.3-28.7-64-64-64H64zM337 209L209 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L303 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z", viewbox: "0 0 448 512" }
  , settings: { path: "M495.9 166.6c3.2 8.7 .5 18.4-6.4 24.6l-43.3 39.4c1.1 8.3 1.7 16.8 1.7 25.4s-.6 17.1-1.7 25.4l43.3 39.4c6.9 6.2 9.6 15.9 6.4 24.6c-4.4 11.9-9.7 23.3-15.8 34.3l-4.7 8.1c-6.6 11-14 21.4-22.1 31.2c-5.9 7.2-15.7 9.6-24.5 6.8l-55.7-17.7c-13.4 10.3-28.2 18.9-44 25.4l-12.5 57.1c-2 9.1-9 16.3-18.2 17.8c-13.8 2.3-28 3.5-42.5 3.5s-28.7-1.2-42.5-3.5c-9.2-1.5-16.2-8.7-18.2-17.8l-12.5-57.1c-15.8-6.5-30.6-15.1-44-25.4L83.1 425.9c-8.8 2.8-18.6 .3-24.5-6.8c-8.1-9.8-15.5-20.2-22.1-31.2l-4.7-8.1c-6.1-11-11.4-22.4-15.8-34.3c-3.2-8.7-.5-18.4 6.4-24.6l43.3-39.4C64.6 273.1 64 264.6 64 256s.6-17.1 1.7-25.4L22.4 191.2c-6.9-6.2-9.6-15.9-6.4-24.6c4.4-11.9 9.7-23.3 15.8-34.3l4.7-8.1c6.6-11 14-21.4 22.1-31.2c5.9-7.2 15.7-9.6 24.5-6.8l55.7 17.7c13.4-10.3 28.2-18.9 44-25.4l12.5-57.1c2-9.1 9-16.3 18.2-17.8C227.3 1.2 241.5 0 256 0s28.7 1.2 42.5 3.5c9.2 1.5 16.2 8.7 18.2 17.8l12.5 57.1c15.8 6.5 30.6 15.1 44 25.4l55.7-17.7c8.8-2.8 18.6-.3 24.5 6.8c8.1 9.8 15.5 20.2 22.1 31.2l4.7 8.1c6.1 11 11.4 22.4 15.8 34.3zM256 336a80 80 0 1 0 0-160 80 80 0 1 0 0 160z", viewbox: "0 0 512 512" }
  , sort: { path: "M137.4 41.4c12.5-12.5 32.8-12.5 45.3 0l128 128c9.2 9.2 11.9 22.9 6.9 34.9s-16.6 19.8-29.6 19.8H32c-12.9 0-24.6-7.8-29.6-19.8s-2.2-25.7 6.9-34.9l128-128zm0 429.3l-128-128c-9.2-9.2-11.9-22.9-6.9-34.9s16.6-19.8 29.6-19.8H288c12.9 0 24.6 7.8 29.6 19.8s2.2 25.7-6.9 34.9l-128 128c-12.5 12.5-32.8 12.5-45.3 0z", viewbox: "0 0 320 512" }
  }

createSvg :: SVG -> String -> String
createSvg { path, viewbox } fill =
  "data:image/svg+xml;utf8,"
    <> "<svg "
    <> "fill='"
    <> replace (Pattern "#") (Replacement "%23") fill
    <> "' "
    <> "xmlns='http://www.w3.org/2000/svg' "
    <> "preserveAspectRatio='xMidYMid meet' "
    <> "viewBox='"
    <> viewbox
    <> "'>"
    <> "<path d='"
    <> path
    <> "' /></svg>"

buttonSvg :: String -> SVG -> C.StyleM Unit
buttonSvg class_ svg = do
  C.select
    (C.Selector (C.byClass class_) C.Star) $
    ( do
        (C.backgroundImage $ C.url $ createSvg svg (Col.toHexString colors.bg))
        if class_ == classes.buttonEdit then
          C.backgroundSize $ C.by (C.px 16.0) (C.px 16.0)
        else
          C.backgroundSize C.contain
    )
  C.select (CS.with ((C.Selector (C.byClass class_) C.Star)) (C.pseudo "hover"))
    ( do
        (C.backgroundImage $ C.url $ createSvg svg (Col.toHexString colors.hover))
    )

component :: forall q i o m. H.Component q i o m
component =
  H.mkComponent
    { initialState: const unit
    , render
    , eval: H.mkEval $ H.defaultEval
    }

render :: forall m. Unit -> H.ComponentHTML Unit () m
render _ =
  HS.stylesheet $ do
    C.select (C.html)
      ( do
          Overflow.overflowY Overflow.overflowAuto
          C.padding (C.px 10.0) (C.px 10.0) (C.px 10.0) (C.px 10.0)
      )
    -- file view
    -- file view div
    C.select (C.Selector (C.byClass classes.fileView) C.Star)
      ( do
          C.display C.flex
          C.flexDirection C.column
      )
    -- file view error span
    C.select (C.Selector (C.byClass classes.fileView) C.Star |> C.span)
      ( do
          C.marginTop $ C.px 10.0
      )

    -- navigation
    -- everything inside the navigation div
    C.select (C.Selector (C.byClass classes.navigation) C.Star)
      ( do
          C.position (C.Position $ C.fromString "sticky")
          C.top (C.px 4.0)
          C.marginBottom (C.px 10.0)
          C.display C.flex
          C.alignItems $ Flexbox.AlignItemsValue Common.center
      )
    -- right part of navigation
    C.select (C.Selector (C.byClass classes.navigationRight) C.Star)
      ( do
          C.marginLeft Common.auto
          C.marginRight $ C.px 0.0
      )

    -- list
    -- a single list item
    C.select (C.Selector (C.byClass classes.listItem) C.Star)
      ( do
          Overflow.overflow Overflow.hidden
          C.width $ C.fromString "fit-content"
          C.height $ C.fromString "fit-content"
          C.marginBottom $ C.px 4.0
          C.alignItems $ Flexbox.AlignItemsValue Common.center
      )
    -- list item label
    C.select (C.Selector (C.byClass classes.listItem) C.Star |> C.span)
      ( do
          C.display C.inlineBlock
          C.minWidth $ C.px 100.0
          C.minHeight $ C.px 16.0
          C.borderRadius (C.px 20.0) (C.px 20.0) (C.px 20.0) (C.px 20.0)
          C.padding (C.px 4.0) (C.px 4.0) (C.px 4.0) (C.px 4.0)
      )
    -- list item label hover
    C.select (CS.with (C.Selector (C.byClass classes.listItem) C.Star |> C.span) (C.pseudo "hover"))
      ( do
          C.backgroundColor colors.hover
          C.cursor Cursor.pointer
      )
    C.select (C.Selector (C.byClass classes.dim) C.Star)
      ( do
          C.color $ Col.graytone 0.5
          C.marginBottom $ C.px 15.0
      )

    -- edit view
    -- everything in the dropdown div
    C.select (C.Selector (C.byClass classes.dropdown) C.Star)
      ( do
          C.display C.inlineBlock
          C.position C.relative
      )
    -- everything in the dropdown content div
    C.select (C.Selector (C.byClass classes.dropdownContent) C.Star)
      ( do
          C.backgroundColor colors.bg
          C.position C.absolute
          C.right $ C.px 0.0
          C.minWidth $ C.px 120.0
          C.zIndex 1
          C.borderRadius (C.px 4.0) (C.px 4.0) (C.px 4.0) (C.px 4.0)
          C.border C.solid (C.px 0.0) colors.bg
      )
    C.select (C.Selector (C.byClass classes.dropdownSettings) C.Star)
      ( do
          C.padding (C.px 10.0) (C.px 10.0) (C.px 10.0) (C.px 10.0)
          C.minWidth $ C.px 325.0
      )
    C.select (C.Selector (C.byClass classes.dropdownSettings) C.Star |* C.label)
      ( do
          C.display C.inlineBlock
          C.width (C.px 140.0)
          CT.textAlign CT.leftTextAlign
      )
    -- a's in the dropdown content div
    C.select (C.Selector (C.byClass classes.dropdownContent) C.Star |* C.a)
      ( do
          C.display C.block
          C.color Col.black
          C.textDecoration C.noneTextDecoration
          C.paddingTop $ C.px 4.0
          CT.textAlign CT.center
      )
    -- hover for a's in the dropdown content div
    C.select (CS.with ((C.Selector (C.byClass classes.dropdownContent) C.Star |* C.a)) (C.pseudo "hover"))
      ( do
          C.backgroundColor colors.hover
          C.cursor Cursor.pointer
      )
    -- boxed divs
    C.select (C.Selector (C.byClass classes.boxed) C.Star)
      ( do
          C.border C.solid (C.px 1.0) colors.bg
          C.padding (C.px 4.0) (C.px 4.0) (C.px 4.0) (C.px 4.0)
          C.borderRadius (C.px 4.0) (C.px 4.0) (C.px 4.0) (C.px 4.0)
          C.marginBottom $ C.px 4.0
      )
    -- labels in the forms
    C.select (C.Selector (C.byClass classes.form) C.Star |* C.label)
      ( do
          C.display C.inlineBlock
          C.width (C.px 140.0)
          CT.textAlign CT.leftTextAlign
      )
    -- a single formfield div
    C.select (C.Selector (C.byClass classes.formField) C.Star)
      ( do
          C.alignItems $ Flexbox.AlignItemsValue Common.center
      )
    -- labels in the formfield
    C.select (C.Selector (C.byClass classes.formParam) C.Star |* C.label)
      ( do
          C.fontWeight C.bold
      )
    -- inputs in the formfield
    C.select (C.Selector (C.byClass classes.formField) C.Star |* C.input)
      ( do
          C.margin (C.px 2.0) (C.px 2.0) (C.px 2.0) (C.px 2.0)
      )
    -- textarea in the formfield
    C.select (C.Selector (C.byClass classes.formField) C.Star |* C.textarea)
      ( do
          C.minWidth $ C.px 249.0
      )
    -- the form name
    C.select (C.Selector (C.byClass classes.formName) C.Star)
      ( do
          C.margin (C.px 0.0) (C.px 0.0) (C.px 0.0) (C.px 0.0)
          C.padding (C.px 0.0) (C.px 0.0) (C.px 0.0) (C.px 0.0)
          C.display C.inlineBlock
          C.alignItems $ Flexbox.AlignItemsValue Common.center
      )
    -- inputs in the form name
    C.select (C.Selector (C.byClass classes.formName) C.Star |* C.input)
      ( do
          C.fontWeight C.bold
          C.fontSize $ C.px 22.0
          C.border (C.Stroke $ Common.none) (C.px 0.0) colors.bg
          C.outline (C.Stroke $ Common.none) (C.px 0.0) colors.bg
      )
    -- invalid fields
    C.select (C.Selector (C.byClass classes.fieldInvalid) C.Star)
      ( do
          C.backgroundColor colors.invalid
      )
    -- the left field in the form
    C.select (C.Selector (C.byClass classes.fieldName) C.Star)
      ( do
          C.width (C.px 137.0)
      )
    -- the group field
    C.select (C.Selector (C.byClass classes.fieldGroup) C.Star |* C.label)
      ( do
          C.color $ Col.graytone 0.5
          C.display C.inlineBlock
          C.width (C.px 140.0)
          CT.textAlign CT.leftTextAlign
      )

    -- buttons
    C.select C.button
      ( do
          C.backgroundRepeat C.noRepeat
          C.backgroundPosition $ C.placed C.sideCenter C.sideCenter
          VA.verticalAlign VA.Middle
          C.width $ C.px 32.0
          C.height $ C.px 32.0
          C.padding (C.px 0.0) (C.px 0.0) (C.px 0.0) (C.px 0.0)
          C.margin (C.px 0.0) (C.px 0.0) (C.px 0.0) (C.px 5.0)
          C.border (C.Stroke $ Common.none) (C.px 0.0) colors.bg
          C.backgroundColor colors.transparent
          C.cursor Cursor.pointer
      )
    C.select (C.Selector (C.byClass classes.buttonSmall) C.Star) $
      ( do
          C.width $ C.px 24.0
          C.height $ C.px 24.0
      )
    C.select (C.Selector (C.byClass classes.buttonExtraSmall) C.Star) $
      ( do
          C.width $ C.px 14.0
          C.height $ C.px 14.0
      )
    buttonSvg classes.buttonAdd svgPaths.add
    buttonSvg classes.buttonAddAddr svgPaths.address
    buttonSvg classes.buttonAddProp svgPaths.property
    buttonSvg classes.buttonBack svgPaths.back
    buttonSvg classes.buttonEdit svgPaths.edit
    buttonSvg classes.buttonHelp svgPaths.help
    buttonSvg classes.buttonRemove svgPaths.remove
    buttonSvg classes.buttonSave svgPaths.save
    buttonSvg classes.buttonSelect svgPaths.select
    buttonSvg classes.buttonSettings svgPaths.settings
    buttonSvg classes.buttonSort svgPaths.sort
