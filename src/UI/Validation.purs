-- File Name: Validation.purs
-- Description: Input validation
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 28 Okt 2023 15:32:42
-- Last Modified: 08 Nov 2023 19:07:55

module UI.Validation where

import Prelude

import Data.Array (any, length)
import Data.CodePoint.Unicode (isAsciiUpper, isDecDigit)
import Data.String.CodePoints (singleton, toCodePointArray)
import Data.Tuple (Tuple(..))

type Messages =
  { propertyName :: String
  , parameter :: String
  , phoneNumber :: String
  , group :: String
  , version :: String
  }

messages :: Messages
messages =
  { propertyName: "can only contain [A-Z | -] and not be [N | FN | ADR]"
  , parameter: "can only be empty or contain [A-Z | -]"
  , phoneNumber: "can only contain [0-9 | + | - | ( | )]"
  , group: "has to be empty or a single uppercase letter"
  , version: "has to be one of [2.1 | 3 | 4]"
  }

-- Is valid
valid :: String -> Tuple Boolean String
valid _ = Tuple true ""

-- Non empty
nonEmpty :: String -> Tuple Boolean String
nonEmpty input = Tuple (input /= "") "cannot be empty"

-- valid vcard version
validVersion :: String -> Tuple Boolean String
validVersion input =
  case input of
    "2.1" -> Tuple true ""
    "3" -> Tuple true ""
    "3.0" -> Tuple true ""
    "4" -> Tuple true ""
    "4.0" -> Tuple true ""
    _ -> Tuple false messages.version

-- Property names should only contain ASCII uppercase letters or the minus symbol
validPropertyName :: String -> Tuple Boolean String
validPropertyName input =
  Tuple (l && not invalid && (not $ any (\c -> c == false) booleans)) messages.propertyName
  where
  arr = toCodePointArray input
  booleans = flip map arr \c -> ((isAsciiUpper c) || (singleton c) == "-")
  invalid = input == "N" || input == "ADR" || input == "FN"
  l = (length arr) /= 0

-- Parameters should only contain ASCII uppercase letters or the minus symbol
validParameter :: String -> Tuple Boolean String
validParameter input =
  Tuple (not $ any (\c -> c == false) booleans) messages.parameter
  where
  arr = toCodePointArray input
  booleans = map (\c -> (isAsciiUpper c) || (singleton c) == "-") arr

-- Phone numbers should only contain digits, minus, plus or parentheses
validPhoneNumber :: String -> Tuple Boolean String
validPhoneNumber input =
  Tuple (not $ any (\c -> c == false) booleans) messages.phoneNumber
  where
  booleans = flip map (toCodePointArray input) \c ->
    (isDecDigit c)
      || (singleton c) == "-"
      || (singleton c) == "+"
      || (singleton c) == "("
      || (singleton c) == ")"

-- Group names can only be a single ASCII uppercase letter
validGroup :: String -> Tuple Boolean String
validGroup input =
  Tuple (l && (not $ any (\c -> c == false) booleans)) messages.group
  where
  arr = toCodePointArray input
  booleans = map (\c -> isAsciiUpper c) arr
  l = (length arr) == 1 || (length arr) == 0
