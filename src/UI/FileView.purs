-- File Name: FileView.purs
-- Description: FileUpload view
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 24 Okt 2023 12:04:47
-- Last Modified: 23 Nov 2023 15:24:34

module UI.FileView where

import Prelude

import DOM.HTML.Indexed.InputAcceptType (InputAcceptTypeAtom(..))
import Data.Const (Const)
import Data.Maybe (Maybe(..))
import Data.String (Pattern(..), Replacement(..), replace)
import Data.Tuple (Tuple(..))
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import UI.Components.HelpButton as ChildHelp
import Type.Proxy (Proxy(..))
import UI.Style (classes)
import Web.File.File (File, toBlob, name)
import Web.File.FileReader.Aff (readAsText)

data Action
  = FileChoose (Maybe File)
  | ShowError String

data Message
  = Content (Tuple String String)
  | NoContent

data Query a = SendError String a

type State =
  { err :: String
  }

type Slots = (help :: H.Slot (Const Void) Unit Int)
_help = Proxy :: Proxy "help"

component :: forall i m. MonadAff m => H.Component Query i Message m
component =
  H.mkComponent
    { initialState: \_ -> { err: "" }
    , render
    , eval: H.mkEval $ H.defaultEval
        { handleAction = handleAction
        , handleQuery = handleQuery
        }
    }

render :: forall m. State -> H.ComponentHTML Action Slots m
render state =
  case state.err of
    "" -> HH.div_
      [ HH.div [ HP.class_ (HH.ClassName classes.navigation) ]
          [ HH.div [ HP.class_ (HH.ClassName classes.navigationLeft) ]
              [ HH.div [ HP.class_ (HH.ClassName classes.fileView) ]
                  [ input ]
              ]
          , HH.div [ HP.class_ (HH.ClassName classes.navigationRight) ]
              [ HH.slot_ _help 0 (ChildHelp.component) unit ]
          ]
      ]
    _ -> HH.div_
      [ HH.div [ HP.class_ (HH.ClassName classes.navigation) ]
          [ HH.div [ HP.class_ (HH.ClassName classes.navigationLeft) ]
              [ HH.div [ HP.class_ (HH.ClassName classes.fileView) ]
                  [ input
                  , HH.span_ [ HH.text state.err ]
                  ]
              ]
          , HH.div [ HP.class_ (HH.ClassName classes.navigationRight) ]
              [ HH.slot_ _help 0 (ChildHelp.component) unit ]
          ]
      ]
  where
  input = HH.input
    [ HP.type_ HP.InputFile
    , HP.class_ (HH.ClassName classes.fileSelector)
    , HP.accept $ HP.InputAcceptType $ [ AcceptFileExtension ".vcf" ]
    , HE.onFileUpload \f -> FileChoose f
    ]

handleAction :: forall m. MonadAff m => Action -> H.HalogenM State Action Slots Message m Unit
handleAction action = case action of
  FileChoose f → case f of
    Just file -> do
      let filename = name file
      let stem = replace (Pattern ".vcf") (Replacement "") filename
      content ← H.liftAff $ readAsText (toBlob file)
      H.raise (Content $ Tuple content stem)
    Nothing -> H.raise NoContent
  ShowError err -> H.modify_ \s -> s { err = err }

handleQuery :: forall a m. Query a -> H.HalogenM State Action Slots Message m (Maybe a)
handleQuery query = case query of
  SendError err next -> do
    H.modify_ \s -> s { err = err }
    pure (Just next)
