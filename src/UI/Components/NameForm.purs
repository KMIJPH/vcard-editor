-- File Name: NameForm.purs
-- Description: Name form
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 26 Okt 2023 11:25:20
-- Last Modified: 09 Nov 2023 11:07:40

module UI.Components.NameForm where

import Prelude

import Data.Const (Const)
import Data.Tuple (Tuple(..))
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Properties as HP
import Type.Proxy (Proxy(..))
import UI.Components.FormField as ChildField
import UI.Style (classes)
import UI.Validation (valid)
import VCF.Type.Name (Name(..))
import VCF.Type.Property (Property(..))

type State =
  { prefix :: String
  , family :: String
  , given :: String
  , add :: String
  , suffix :: String
  }

data Action
  = DoNothing
  | UpdateField (Tuple String Int)

data Message = UpdateName (Property Name)

type Slots = (field :: H.Slot (Const Void) ChildField.Message Int)

_field = Proxy :: Proxy "field"

initialState :: Property Name -> State
initialState (Property p) =
  { prefix: n.prefix, family: n.family, given: n.given, add: n.add, suffix: n.suffix }
  where

  name :: Name -> { family :: String, given :: String, add :: String, prefix :: String, suffix :: String }
  name (Name nn) = nn

  n = name p.value

component :: forall q m. H.Component q (Property Name) Message m
component =
  H.mkComponent
    { initialState: initialState
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction }
    }

render :: forall m. State -> H.ComponentHTML Action Slots m
render state =
  HH.div [ HP.class_ (HH.ClassName classes.boxed) ]
    [ HH.div [ HP.class_ (HH.ClassName classes.formName) ]
        [ HH.input
            [ HP.type_ HP.InputText
            , HP.value "N"
            , HP.readOnly true
            , HP.tabIndex (-1)
            ]
        ]
    , HH.div_
        [ HH.div [ HP.class_ (HH.ClassName classes.form) ]
            [ HH.slot _field 0 (ChildField.component)
                { name: "Prefix"
                , value: state.prefix
                , index: 0
                , isParameter: false
                , isTextArea: false
                , sug: false
                , className: classes.formField
                , validation: Tuple valid valid
                }
                listenField
            , HH.slot _field 2 (ChildField.component)
                { name: "Given"
                , value: state.given
                , index: 2
                , isParameter: false
                , isTextArea: false
                , sug: false
                , className: classes.formField
                , validation: Tuple valid valid
                }
                listenField
            , HH.slot _field 3 (ChildField.component)
                { name: "Additional"
                , value: state.add
                , index: 3
                , isParameter: false
                , isTextArea: false
                , sug: false
                , className: classes.formField
                , validation: Tuple valid valid
                }
                listenField
            , HH.slot _field 1 (ChildField.component)
                { name: "Family"
                , value: state.family
                , index: 1
                , isParameter: false
                , isTextArea: false
                , sug: false
                , className: classes.formField
                , validation: Tuple valid valid
                }
                listenField
            , HH.slot _field 4 (ChildField.component)
                { name: "Suffix"
                , value: state.suffix
                , index: 4
                , isParameter: false
                , isTextArea: false
                , sug: false
                , className: classes.formField
                , validation: Tuple valid valid
                }
                listenField
            ]
        ]
    ]

listenField :: ChildField.Message -> Action
listenField message =
  case message of
    ChildField.RemoveField _ -> DoNothing
    ChildField.SendName _ -> DoNothing
    ChildField.SendValidity _ -> DoNothing
    ChildField.SendValue (Tuple value i) -> UpdateField $ Tuple value i

handleAction :: forall m. Action -> H.HalogenM State Action Slots Message m Unit
handleAction action = case action of
  DoNothing -> pure unit
  UpdateField (Tuple value i) -> do
    state <- case i of
      0 -> H.modify \s -> s { prefix = value }
      1 -> H.modify \s -> s { family = value }
      2 -> H.modify \s -> s { given = value }
      3 -> H.modify \s -> s { add = value }
      4 -> H.modify \s -> s { suffix = value }
      _ -> H.get
    H.raise
      $ UpdateName
      $ Property
          { name: "N"
          , group: ""
          , params: []
          , value: Name
              { prefix: state.prefix
              , family: state.family
              , given: state.given
              , add: state.add
              , suffix: state.suffix
              }
          }
