-- File Name: HelpButton.purs
-- Description: Help button (with dropdown)
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 30 Okt 2023 15:23:42
-- Last Modified: 23 Nov 2023 11:13:02

module UI.Components.HelpButton where

import Prelude

import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import UI.Style (classes)

type State = { isOpen :: Boolean }

data Action
  = Open
  | Close

component :: forall q o m. H.Component q Unit o m
component =
  H.mkComponent
    { initialState: \_ -> { isOpen: false }
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction }
    }

render :: forall m. State -> H.ComponentHTML Action () m
render state =
  case state.isOpen of
    false -> HH.button
      [ HP.class_ $ HH.ClassName classes.buttonHelp
      , HP.title "help"
      , HE.onMouseOver \_ -> Open
      ]
      []
    true -> HH.div [ HP.class_ $ HH.ClassName classes.dropdown, HE.onMouseLeave \_ -> Close ]
      [ HH.button
          [ HP.class_ $ HH.ClassName classes.buttonHelp
          , HP.title "help"
          ]
          []
      , HH.div [ HP.class_ $ HH.ClassName classes.dropdownContent ]
          [ HH.a [ HP.href "https://codeberg.org/KMIJPH/vcard-editor/src/branch/main/etc/vcard-21.pdf", HP.target "_blank" ] [ HH.text "vCard 2.1 spec" ]
          , HH.a [ HP.href "https://www.rfc-editor.org/rfc/rfc2426", HP.target "_blank" ] [ HH.text "vCard 3 spec" ]
          , HH.a [ HP.href "https://www.rfc-editor.org/rfc/rfc6350", HP.target "_blank" ] [ HH.text "vCard 4 spec" ]
          , HH.a [ HP.href "https://codeberg.org/KMIJPH/vcard-editor/src/branch/main/README.md", HP.target "_blank" ] [ HH.text "Help" ]
          ]
      ]

handleAction :: forall o m. Action -> H.HalogenM State Action () o m Unit
handleAction action = case action of
  Open -> H.modify_ \s -> s { isOpen = true }
  Close -> H.modify_ \s -> s { isOpen = false }
