-- -- File Name: ListItem.purs
-- -- Description: A single list item
-- -- Author: KMIJPH
-- -- Repository: https://codeberg.org/KMIJPH
-- -- License: GPL3
-- -- Creation Date: 24 Okt 2023 13:24:19
-- -- Last Modified: 08 Nov 2023 16:42:19

module UI.Components.ListItem where

import Prelude

import Data.Maybe (Maybe(..))
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import UI.Style (classes)

data Action
  = Edit
  | Receive ListInput
  | Select Boolean

data Message = GotoEdit Int

data Query a
  = IsSelected (Boolean -> a)
  | SelectItem Boolean a

type ListInput =
  { label :: String
  , index :: Int
  }

type State =
  { label :: String
  , index :: Int
  , selected :: Boolean
  }

initialState :: ListInput -> State
initialState { label, index } =
  { label: label
  , index: index
  , selected: false
  }

component :: forall m. H.Component Query ListInput Message m
component =
  H.mkComponent
    { initialState: initialState
    , render
    , eval: H.mkEval $ H.defaultEval
        { handleAction = handleAction
        , handleQuery = handleQuery
        , receive = Just <<< Receive -- redraw
        }
    }

render :: forall m. State -> H.ComponentHTML Action () m
render state =
  HH.div [ HP.class_ $ HH.ClassName classes.listItem ]
    [ HH.input [ HP.class_ $ HH.ClassName classes.listCheckBox, HP.type_ HP.InputCheckbox, HP.checked state.selected, HE.onChecked \b -> Select b ]
    , HH.span [ HE.onClick \_ -> Edit ] [ HH.text $ state.label ]
    ]

handleAction :: forall m. Action -> H.HalogenM State Action () Message m Unit
handleAction action = case action of
  Edit -> do
    state <- H.get
    H.raise (GotoEdit state.index)
  Receive { label, index } -> H.modify_ \s -> s { label = label, index = index, selected = false }
  Select b -> H.modify_ \s -> s { selected = b }

handleQuery :: forall a m. Query a -> H.HalogenM State Action () Message m (Maybe a)
handleQuery query = case query of
  IsSelected reply -> do
    state <- H.get
    pure (Just (reply state.selected))
  SelectItem b next -> do
    H.modify_ \s -> s { selected = b }
    pure (Just next)
