-- File Name: FormField.purs
-- Description: Single form field (name, value)
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 25 Okt 2023 16:52:02
-- Last Modified: 09 Nov 2023 11:02:45

module UI.Components.FormField where

import Prelude

import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..), fst, snd)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import UI.Style (classes, ids)

data Action
  = Initialize
  | Receive FormInput
  | Remove
  | UpdateName String
  | UpdateValue String

data Message
  = RemoveField Int
  | SendName (Tuple String Int)
  | SendValidity Boolean
  | SendValue (Tuple String Int)

type FormInput =
  { className :: String
  , index :: Int
  , isParameter :: Boolean
  , isTextArea :: Boolean
  , name :: String
  , sug :: Boolean
  , validation :: Tuple (String -> Tuple Boolean String) (String -> Tuple Boolean String)
  , value :: String
  }

type State =
  { className :: String
  , index :: Int
  , isParameter :: Boolean
  , isTextArea :: Boolean
  , isValid :: Boolean
  , msgName :: String
  , msgValue :: String
  , name :: String
  , sug :: Boolean
  , validation :: Tuple (String -> Tuple Boolean String) (String -> Tuple Boolean String)
  , value :: String
  }

initialState :: FormInput -> State
initialState { name, value, index, isParameter, validation, className, isTextArea, sug } =
  { name
  , value
  , index
  , isParameter
  , validation
  , className
  , isTextArea
  , sug
  , isValid: true
  , msgName: ""
  , msgValue: ""
  }

component :: forall q m. H.Component q FormInput Message m
component =
  H.mkComponent
    { initialState: initialState
    , render
    , eval: H.mkEval $ H.defaultEval
        { handleAction = handleAction
        , initialize = Just Initialize
        , receive = Just <<< Receive -- redraw
        }
    }

render :: forall m. State -> H.ComponentHTML Action () m
render state =
  case state.isParameter of
    false -> renderGeneric
    true -> renderParameter
  where

  renderGeneric :: forall mm. H.ComponentHTML Action () mm
  renderGeneric =
    case state.isValid of
      true ->
        case state.isTextArea of
          true ->
            HH.div [ HP.class_ $ HH.ClassName state.className ]
              [ HH.label_ [ HH.text (state.name) ]
              , HH.textarea
                  [ HP.value (state.value)
                  , HE.onValueChange \s -> UpdateValue s
                  ]
              ]
          false ->
            HH.div [ HP.class_ $ HH.ClassName state.className ]
              [ HH.label_ [ HH.text (state.name) ]
              , HH.input
                  [ HP.value (state.value)
                  , HP.type_ HP.InputText
                  , HP.list (if state.sug then ids.propSuggestions else "")
                  , HE.onValueChange \s -> UpdateValue s
                  ]
              ]
      false ->
        case state.isTextArea of
          true -> HH.div [ HP.class_ $ HH.ClassName state.className ]
            [ HH.label_ [ HH.text (state.name) ]
            , HH.textarea
                [ HP.class_ (HH.ClassName classes.fieldInvalid)
                , HP.title state.msgValue
                , HP.value (state.value)
                , HE.onValueChange \s -> UpdateValue s
                ]
            ]
          false -> HH.div [ HP.class_ $ HH.ClassName state.className ]
            [ HH.label_ [ HH.text (state.name) ]
            , HH.input
                [ HP.class_ (HH.ClassName classes.fieldInvalid)
                , HP.type_ HP.InputText
                , HP.list (if state.sug then ids.propSuggestions else "")
                , HP.title state.msgValue
                , HP.value (state.value)
                , HE.onValueChange \s -> UpdateValue s
                ]
            ]

  renderParameter :: forall mm. H.ComponentHTML Action () mm
  renderParameter =
    case state.isValid of
      true ->
        HH.div [ HP.class_ $ HH.ClassName state.className ]
          [ HH.input
              [ HP.class_ $ HH.ClassName classes.fieldName
              , HP.type_ HP.InputText
              , HP.list ids.nameSuggestions
              , HP.value (state.name)
              , HP.placeholder "Param Name"
              , HE.onValueChange \s -> UpdateName s
              ]
          , HH.input
              [ HP.value (state.value)
              , HP.type_ HP.InputText
              , HP.list ids.valueSuggestions
              , HP.placeholder "Param Value"
              , HE.onValueChange \s -> UpdateValue s
              ]
          , HH.button
              [ HP.classes
                  [ HH.ClassName classes.buttonRemove
                  , HH.ClassName classes.buttonSmall
                  ]
              , HP.title "remove"
              , HE.onClick \_ -> Remove
              ]
              []
          ]
      false ->
        HH.div [ HP.class_ $ HH.ClassName state.className ]
          [ invalidName
          , invalidValue
          , HH.button
              [ HP.classes
                  [ HH.ClassName classes.buttonRemove
                  , HH.ClassName classes.buttonSmall
                  ]
              , HP.title "remove"
              , HE.onClick \_ -> Remove
              ]
              []
          ]
    where
    invalidName = case state.msgName of
      "" -> HH.input
        [ HP.class_ $ HH.ClassName classes.fieldName
        , HP.type_ HP.InputText
        , HP.list ids.nameSuggestions
        , HP.value state.name
        , HP.placeholder "Param Name"
        , HE.onValueChange \s -> UpdateName s
        ]
      _ -> HH.input
        [ HP.classes [ HH.ClassName classes.fieldName, HH.ClassName classes.fieldInvalid ]
        , HP.type_ HP.InputText
        , HP.list ids.nameSuggestions
        , HP.title state.msgName
        , HP.value (state.name)
        , HP.placeholder "Param Name"
        , HE.onValueChange \s -> UpdateName s
        ]
    invalidValue = case state.msgValue of
      "" -> HH.input
        [ HP.value (state.value)
        , HP.type_ HP.InputText
        , HP.list ids.valueSuggestions
        , HP.placeholder "Param Value"
        , HE.onValueChange \s -> UpdateValue s
        ]
      _ -> HH.input
        [ HP.class_ $ HH.ClassName classes.fieldInvalid
        , HP.type_ HP.InputText
        , HP.list ids.valueSuggestions
        , HP.title state.msgValue
        , HP.value (state.value)
        , HP.placeholder "Param Value"
        , HE.onValueChange \s -> UpdateValue s
        ]

validateName :: forall m. String -> H.HalogenM State Action () Message m Unit
validateName name = do
  state <- H.get
  let validation = fst state.validation
  case validation name of
    Tuple true _ -> do
      H.modify_ \s -> s { name = name, isValid = true, msgName = "" }
      H.raise $ SendName $ Tuple name state.index
      H.raise $ SendValidity true
    Tuple false msg -> do
      H.modify_ \s -> s { isValid = false, msgName = msg }
      H.raise $ SendValidity false

validateValue :: forall m. String -> H.HalogenM State Action () Message m Unit
validateValue value = do
  state <- H.get
  let validation = snd state.validation
  case validation value of
    Tuple true _ -> do
      H.modify_ \s -> s { value = value, isValid = true, msgValue = "" }
      H.raise $ SendValidity true
      H.raise $ SendValue $ Tuple value state.index
    Tuple false msg -> do
      H.modify_ \s -> s { isValid = false, msgValue = msg }
      H.raise $ SendValidity false

handleAction :: forall m. Action -> H.HalogenM State Action () Message m Unit
handleAction action = case action of
  Initialize -> do
    state <- H.get
    validateName state.name
    validateValue state.value
  Receive { name, value, index, isParameter, isTextArea } ->
    H.modify_ \s -> s
      { name = name
      , value = value
      , index = index
      , isParameter = isParameter
      , isTextArea = isTextArea
      }
  Remove -> do
    state <- H.get
    H.raise (RemoveField state.index)
  UpdateName name -> validateName name
  UpdateValue value -> validateValue value
