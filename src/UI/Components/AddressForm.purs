-- -- File Name: AddressForm.purs
-- -- Description: Address form
-- -- Author: KMIJPH
-- -- Repository: https://codeberg.org/KMIJPH
-- -- License: GPL3
-- -- Creation Date: 26 Okt 2023 12:04:23
-- -- Last Modified: 09 Nov 2023 11:07:30

module UI.Components.AddressForm where

import Prelude

import Data.Const (Const)
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Type.Proxy (Proxy(..))
import UI.Components.FormField as ChildField
import UI.Components.ParameterField as ChildParam
import UI.Style (classes)
import UI.Validation (valid, validGroup)
import VCF.Type.Address (Address(..))
import VCF.Type.Common (Parameter)
import VCF.Type.Property (Property(..))

data Action
  = DoNothing
  | Receive AddressInput
  | ReceiveValidity Boolean
  | Remove
  | UpdateField (Tuple String Int)
  | UpdateParams (Array Parameter)

data Message
  = RemoveAddress Int
  | SendValidity Boolean
  | UpdateAddress (Tuple (Property Address) Int)

type AddressInput =
  { property :: Property Address
  , index :: Int
  }

type State =
  { adr :: String
  , ext :: String
  , str :: String
  , loc :: String
  , reg :: String
  , postal :: String
  , ctry :: String
  , group :: String
  , params :: Array Parameter
  , index :: Int
  , isValid :: Boolean
  }

type Slots =
  ( field :: H.Slot (Const Void) ChildField.Message Int
  , params :: H.Slot (Const Void) ChildParam.Message Int
  )

_field = Proxy :: Proxy "field"
_params = Proxy :: Proxy "params"

initialState :: AddressInput -> State
initialState { property, index } =
  { adr: a.adr
  , ext: a.ext
  , str: a.str
  , loc: a.loc
  , reg: a.reg
  , postal: a.postal
  , ctry: a.ctry
  , group: p.group
  , params: p.params
  , index: index
  , isValid: true
  }
  where
  prop :: forall a. Property a -> { group :: String, name :: String, params :: Array Parameter, value :: a }
  prop (Property pp) = pp

  addr :: Address -> { adr :: String, ctry :: String, ext :: String, loc :: String, postal :: String, reg :: String, str :: String }
  addr (Address aa) = aa

  p = prop property
  a = addr p.value

component :: forall q m. H.Component q AddressInput Message m
component =
  H.mkComponent
    { initialState: initialState
    , render
    , eval: H.mkEval $ H.defaultEval
        { handleAction = handleAction
        , receive = Just <<< Receive -- redraw
        }
    }

render :: forall m. State -> H.ComponentHTML Action Slots m
render state =
  HH.div [ HP.class_ $ HH.ClassName classes.boxed ]
    [ HH.button
        [ HP.classes [ HH.ClassName classes.buttonRemove, HH.ClassName classes.buttonSmall ]
        , HP.title "remove address"
        , HE.onClick \_ -> Remove
        ]
        []
    , HH.div [ HP.class_ (HH.ClassName classes.formName) ]
        [ HH.input
            [ HP.type_ HP.InputText
            , HP.value "ADR"
            , HP.readOnly true
            , HP.tabIndex (-1)
            ]
        ]
    , HH.div_
        [ HH.div [ HP.class_ $ HH.ClassName classes.fieldGroup ]
            [ HH.slot _field 0 (ChildField.component)
                { name: "Property Group"
                , value: state.group
                , index: 0
                , isParameter: false
                , isTextArea: false
                , sug: false
                , className: classes.formField
                , validation: Tuple valid validGroup
                }
                listenField
            ]
        , HH.div [ HP.class_ (HH.ClassName classes.form) ]
            [ HH.slot _field 1 (ChildField.component)
                { name: "Post Office Address"
                , value: state.adr
                , index: 1
                , isParameter: false
                , isTextArea: false
                , sug: false
                , className: classes.formField
                , validation: Tuple valid valid
                }
                listenField
            , HH.slot _field 2 (ChildField.component)
                { name: "Extended"
                , value: state.ext
                , index: 2
                , isParameter: false
                , isTextArea: false
                , sug: false
                , className: classes.formField
                , validation: Tuple valid valid
                }
                listenField
            , HH.slot _field 3 (ChildField.component)
                { name: "Street"
                , value: state.str
                , index: 3
                , isParameter: false
                , isTextArea: false
                , sug: false
                , className: classes.formField
                , validation: Tuple valid valid
                }
                listenField
            , HH.slot _field 4 (ChildField.component)
                { name: "Locality"
                , value: state.loc
                , index: 4
                , isParameter: false
                , isTextArea: false
                , sug: false
                , className: classes.formField
                , validation: Tuple valid valid
                }
                listenField
            , HH.slot _field 5 (ChildField.component)
                { name: "Region"
                , value: state.reg
                , index: 5
                , isParameter: false
                , isTextArea: false
                , sug: false
                , className: classes.formField
                , validation: Tuple valid valid
                }
                listenField
            , HH.slot _field 6 (ChildField.component)
                { name: "Postal Code"
                , value: state.postal
                , index: 6
                , isParameter: false
                , isTextArea: false
                , sug: false
                , className: classes.formField
                , validation: Tuple valid valid
                }
                listenField
            , HH.slot _field 7 (ChildField.component)
                { name: "Country"
                , value: state.ctry
                , index: 7
                , isParameter: false
                , isTextArea: false
                , sug: false
                , className: classes.formField
                , validation: Tuple valid valid
                }
                listenField
            ]
        , HH.slot _params 0 (ChildParam.component) { items: state.params } listenParam
        ]
    ]

listenField :: ChildField.Message -> Action
listenField message =
  case message of
    ChildField.RemoveField _ -> DoNothing
    ChildField.SendName _ -> DoNothing
    ChildField.SendValidity b -> ReceiveValidity b
    ChildField.SendValue (Tuple value i) -> UpdateField $ Tuple value i

listenParam :: ChildParam.Message -> Action
listenParam message =
  case message of
    ChildParam.SendParameters params -> UpdateParams params
    ChildParam.SendValidity b -> ReceiveValidity b

handleAction :: forall m. Action -> H.HalogenM State Action Slots Message m Unit
handleAction action = case action of
  DoNothing -> pure unit
  Receive input -> H.modify_ \_ -> initialState input
  ReceiveValidity b -> H.raise $ SendValidity b
  Remove -> do
    state <- H.get
    H.raise (RemoveAddress state.index)
  UpdateField (Tuple value i) -> do
    state <- case i of
      0 -> H.modify \s -> s { group = value }
      1 -> H.modify \s -> s { adr = value }
      2 -> H.modify \s -> s { ext = value }
      3 -> H.modify \s -> s { str = value }
      4 -> H.modify \s -> s { loc = value }
      5 -> H.modify \s -> s { reg = value }
      6 -> H.modify \s -> s { postal = value }
      7 -> H.modify \s -> s { ctry = value }
      _ -> H.get
    H.raise
      $ UpdateAddress
      $ Tuple
          ( Property
              { name: "ADR"
              , value: Address
                  { adr: state.adr
                  , ext: state.ext
                  , str: state.str
                  , loc: state.loc
                  , reg: state.reg
                  , postal: state.postal
                  , ctry: state.ctry
                  }
              , group: state.group
              , params: state.params
              }
          )
          state.index
  UpdateParams params -> do
    state <- H.modify \s -> s { params = params }
    H.raise
      $ UpdateAddress
      $ Tuple
          ( Property
              { name: "ADR"
              , value: Address
                  { adr: state.adr
                  , ext: state.ext
                  , str: state.str
                  , loc: state.loc
                  , reg: state.reg
                  , postal: state.postal
                  , ctry: state.ctry
                  }
              , group: state.group
              , params: state.params
              }
          )
          state.index
