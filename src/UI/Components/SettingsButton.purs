-- File Name: SettingsButton.purs
-- Description:
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 03 Nov 2023 14:51:18
-- Last Modified: 14 Nov 2023 22:11:14

module UI.Components.SettingsButton where

import Prelude

import Data.Maybe (Maybe(..))
import Effect.Class (class MonadEffect)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import UI.Style (classes)
import VCF.Encoding (Encodings(..))
import VCF.Type.Name (NameOrder(..))
import VCF.Writer (ExportSettings)
import Web.Event.Event (target)
import Web.HTML.HTMLInputElement (fromEventTarget, value)
import Web.UIEvent.KeyboardEvent (KeyboardEvent, toEvent, key)

type State =
  { isOpen :: Boolean
  , filterName :: String
  , settings :: ExportSettings
  }

type SettingsInput =
  { isOpen :: Boolean
  , settings :: ExportSettings
  }

data Filter = Name String | Org String | Reset

data Action
  = Filter KeyboardEvent
  | Toggle
  | UpdateEncoding String
  | UpdateFilterName String
  | UpdateNameOrder String
  | UpdateVersion String
  | UpdateWrap String

data Message
  = SendFilter Filter
  | SendSettings ExportSettings

initialState :: SettingsInput -> State
initialState { isOpen, settings } =
  { isOpen
  , settings
  , filterName: "name"
  }

component :: forall q m. MonadEffect m => H.Component q SettingsInput Message m
component =
  H.mkComponent
    { initialState: initialState
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction }
    }

render :: forall m. State -> H.ComponentHTML Action () m
render state =
  case state.isOpen of
    false -> HH.button
      [ HP.class_ $ HH.ClassName classes.buttonSettings
      , HP.title "help"
      , HE.onClick \_ -> Toggle
      ]
      []
    true -> HH.div [ HP.class_ $ HH.ClassName classes.dropdown ]
      [ HH.button
          [ HP.class_ $ HH.ClassName classes.buttonSettings
          , HP.title "settings"
          , HE.onClick \_ -> Toggle
          ]
          []
      , HH.div [ HP.classes [ HH.ClassName classes.dropdownContent, HH.ClassName classes.dropdownSettings ] ]
          [ HH.div_
              [ HH.label [ HP.for "encoding" ] [ HH.text "default encoding:" ]
              , HH.select [ HP.name "encoding", HE.onValueChange \v -> UpdateEncoding v ] (encodingOptions state.settings.encoding)
              ]
          , HH.div_
              [ HH.label [ HP.for "fn" ] [ HH.text "FN generation:" ]
              , HH.select [ HP.name "fn", HE.onValueChange \v -> UpdateNameOrder v ] (fnOptions state.settings.nameOrder)
              ]
          , HH.div_
              [ HH.label [ HP.for "version" ] [ HH.text "export version:" ]
              , HH.select [ HP.name "version", HE.onValueChange \v -> UpdateVersion v ] (versionOptions state.settings.version)
              ]
          , HH.div_
              [ HH.label [ HP.for "wrap" ] [ HH.text "wrap:" ]
              , HH.select [ HP.name "wrap", HE.onValueChange \v -> UpdateWrap v ] (wrapOptions state.settings.wrap)
              ]
          , HH.div_
              [ HH.label [ HP.for "filter" ] [ HH.text "filter:" ]
              , HH.select [ HP.name "filter", HE.onValueChange \v -> UpdateFilterName v ] (filterOptions state.filterName)
              , HH.input [ HP.type_ HP.InputText, HE.onKeyDown \ev -> Filter ev ]
              ]
          ]
      ]

handleAction :: forall m. MonadEffect m => Action -> H.HalogenM State Action () Message m Unit
handleAction action = case action of
  Filter event -> do
    state <- H.get
    case target $ toEvent event of
      Just t -> case fromEventTarget t of
        Just input -> do
          val <- H.liftEffect $ value input
          case key event of
            "Enter" -> case state.filterName of
              "name" -> case val of
                "" -> H.raise (SendFilter Reset)
                _ -> H.raise (SendFilter $ Name val)
              _ -> case val of
                "" -> H.raise (SendFilter Reset)
                _ -> H.raise (SendFilter $ Org val)
            _ -> pure unit
        Nothing -> pure unit
      Nothing -> pure unit
  Toggle -> H.modify_ \s -> s { isOpen = not s.isOpen }
  UpdateEncoding v -> do
    state <- H.modify \s ->
      s { settings = { encoding: encoding, nameOrder: s.settings.nameOrder, version: s.settings.version, wrap: s.settings.wrap } }
    H.modify_ \_ -> state
    H.raise (SendSettings state.settings)
    where
    encoding = case v of
      "qp" -> Just QP
      "8bit" -> Just EightBit
      _ -> Nothing
  UpdateFilterName v -> H.modify_ \s -> s { filterName = v }
  UpdateNameOrder v -> do
    state <- H.modify \s -> s
      { settings = { encoding: s.settings.encoding, nameOrder: fn, version: s.settings.version, wrap: s.settings.wrap } }
    H.modify_ \_ -> state
    H.raise (SendSettings state.settings)
    where
    fn = case v of
      "western" -> Western
      _ -> Eastern
  UpdateVersion v -> do
    state <- H.modify \s -> s
      { settings = { encoding: s.settings.encoding, nameOrder: s.settings.nameOrder, version: version, wrap: s.settings.wrap } }
    H.modify_ \_ -> state
    H.raise (SendSettings state.settings)
    where
    version = case v of
      "2.1" -> 2.1
      "3.0" -> 3.0
      _ -> 4.0
  UpdateWrap v -> do
    state <- H.modify \s -> s
      { settings = { encoding: s.settings.encoding, nameOrder: s.settings.nameOrder, version: s.settings.version, wrap: wrap } }
    H.modify_ \_ -> state
    H.raise (SendSettings state.settings)
    where
    wrap = case v of
      "true" -> true
      _ -> false

filterOptions :: forall w i. String -> Array (HH.HTML w i)
filterOptions filt =
  if filt == "name" then
    [ HH.option [ HP.value "name", HP.selected true ] [ HH.text "name" ]
    , HH.option [ HP.value "org" ] [ HH.text "org" ]
    ]
  else
    [ HH.option [ HP.value "name" ] [ HH.text "name" ]
    , HH.option [ HP.value "org", HP.selected true ] [ HH.text "org" ]
    ]

wrapOptions :: forall w i. Boolean -> Array (HH.HTML w i)
wrapOptions wrap =
  if wrap == true then
    [ HH.option [ HP.value "true", HP.selected true ] [ HH.text "true" ]
    , HH.option [ HP.value "false" ] [ HH.text "false" ]
    ]
  else
    [ HH.option [ HP.value "true" ] [ HH.text "true" ]
    , HH.option [ HP.value "false", HP.selected true ] [ HH.text "false" ]
    ]

versionOptions :: forall w i. Number -> Array (HH.HTML w i)
versionOptions version =
  if version == 2.1 then
    [ HH.option [ HP.value "2.1", HP.selected true ] [ HH.text "2.1" ]
    , HH.option [ HP.value "3.0" ] [ HH.text "3.0" ]
    , HH.option [ HP.value "4.0" ] [ HH.text "4.0" ]
    ]
  else if version == 3.0 then
    [ HH.option [ HP.value "2.1" ] [ HH.text "2.1" ]
    , HH.option [ HP.value "3.0", HP.selected true ] [ HH.text "3.0" ]
    , HH.option [ HP.value "4.0" ] [ HH.text "4.0" ]
    ]
  else
    [ HH.option [ HP.value "2.1" ] [ HH.text "2.1" ]
    , HH.option [ HP.value "3.0" ] [ HH.text "3.0" ]
    , HH.option [ HP.value "4.0", HP.selected true ] [ HH.text "4.0" ]
    ]

fnOptions :: forall w i. NameOrder -> Array (HH.HTML w i)
fnOptions order =
  case order of
    Western ->
      [ HH.option [ HP.value "western", HP.selected true ] [ HH.text "western" ]
      , HH.option [ HP.value "eastern" ] [ HH.text "eastern" ]
      ]
    Eastern ->
      [ HH.option [ HP.value "western" ] [ HH.text "western" ]
      , HH.option [ HP.value "eastern", HP.selected true ] [ HH.text "eastern" ]
      ]

encodingOptions :: forall w i. Maybe Encodings -> Array (HH.HTML w i)
encodingOptions encoding =
  case encoding of
    Just QP ->
      [ HH.option [ HP.value "qp", HP.selected true ] [ HH.text "quoted-printable" ]
      , HH.option [ HP.value "8bit" ] [ HH.text "8bit" ]
      , HH.option [ HP.value "none" ] [ HH.text "none" ]
      ]
    Just EightBit ->
      [ HH.option [ HP.value "qp" ] [ HH.text "quoted-printable" ]
      , HH.option [ HP.value "8bit", HP.selected true ] [ HH.text "8bit" ]
      , HH.option [ HP.value "none" ] [ HH.text "none" ]
      ]
    _ ->
      [ HH.option [ HP.value "qp" ] [ HH.text "quoted-printable" ]
      , HH.option [ HP.value "8bit" ] [ HH.text "8bit" ]
      , HH.option [ HP.value "none", HP.selected true ] [ HH.text "none" ]
      ]
