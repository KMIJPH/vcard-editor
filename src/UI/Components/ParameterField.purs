-- File Name: ParameterField.purs
-- Description: Parameter field(s)
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 27 Okt 2023 16:05:50
-- Last Modified: 09 Nov 2023 11:07:10

module UI.Components.ParameterField where

import Prelude

import Data.Array (cons, deleteAt, index, mapWithIndex, snoc, updateAt)
import Data.Const (Const)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Tuple (Tuple(..))
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Type.Proxy (Proxy(..))
import UI.Components.FormField as ChildField
import UI.Style (classes)
import UI.Validation (nonEmpty, validParameter)
import VCF.Type.Common (Parameter(..), empty)

data Action
  = AddParam
  | Receive State
  | ReceiveValidity Boolean
  | RemoveParam Int
  | UpdateParamName (Tuple String Int)
  | UpdateParamValue (Tuple String Int)

data Message
  = SendParameters (Array Parameter)
  | SendValidity Boolean

type State = { items :: Array Parameter }

type Slots = (field :: H.Slot (Const Void) ChildField.Message Int)

_field = Proxy :: Proxy "field"

initialState :: State -> State
initialState { items } = { items }

component :: forall q m. H.Component q State Message m
component =
  H.mkComponent
    { initialState: initialState
    , render
    , eval: H.mkEval $ H.defaultEval
        { handleAction = handleAction
        , receive = Just <<< Receive -- redraw
        }
    }

render :: forall m. State -> H.ComponentHTML Action Slots m
render state =
  HH.div [ HP.class_ (HH.ClassName classes.formParam) ]
    ( cons
        ( HH.div_
            [ HH.label_ [ HH.text "Parameters" ]
            , HH.button
                [ HP.classes
                    [ HH.ClassName classes.buttonAdd
                    , HH.ClassName classes.buttonExtraSmall
                    ]
                , HP.title "add parameter"
                , HE.onClick \_ -> AddParam
                ]
                []
            ]
        )
        ( flip mapWithIndex state.items \i (Parameter param) ->
            HH.slot _field i (ChildField.component)
              { name: param.param
              , value: param.value
              , index: i
              , isParameter: true
              , isTextArea: false
              , sug: true
              , className: classes.formField
              , validation: Tuple validParameter nonEmpty
              }
              listen
        )
    )

listen :: ChildField.Message -> Action
listen message = case message of
  ChildField.RemoveField i -> RemoveParam i
  ChildField.SendName (Tuple name i) -> UpdateParamName $ Tuple name i
  ChildField.SendValidity b -> ReceiveValidity b
  ChildField.SendValue (Tuple value i) -> UpdateParamValue $ Tuple value i

handleAction :: forall m. Action -> H.HalogenM State Action Slots Message m Unit
handleAction action = case action of
  AddParam -> do
    state <- H.modify \s -> s { items = snoc s.items empty }
    H.raise (SendParameters state.items)
  Receive { items } -> H.modify_ \s -> s { items = items }
  ReceiveValidity b -> H.raise $ SendValidity b
  RemoveParam i -> do
    state <- H.modify \s -> s { items = fromMaybe s.items $ deleteAt i s.items }
    H.raise (SendParameters state.items)
  UpdateParamName (Tuple name i) -> do
    state <- H.get
    newState <- case index state.items i of
      Just (Parameter param) -> H.modify \s -> s { items = fromMaybe s.items $ updateAt i (Parameter { param: name, value: param.value }) state.items }
      Nothing -> H.get
    H.raise (SendParameters newState.items)
  UpdateParamValue (Tuple value i) -> do
    state <- H.get
    newState <- case index state.items i of
      Just (Parameter param) -> H.modify \s -> s { items = fromMaybe s.items $ updateAt i (Parameter { param: param.param, value: value }) state.items }
      Nothing -> H.get
    H.raise (SendParameters newState.items)
