-- File Name: PropertyForm.purs
-- Description: Property form
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 26 Okt 2023 12:04:23
-- Last Modified: 14 Nov 2023 22:38:01

module UI.Components.PropertyForm where

import Prelude

import Data.Const (Const)
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Type.Proxy (Proxy(..))
import UI.Components.FormField as ChildField
import UI.Components.ParameterField as ChildParam
import UI.Style (classes, ids)
import UI.Validation (valid, validGroup, validPhoneNumber, validPropertyName)
import VCF.Type.Common (Parameter, Value(..), toOther)
import VCF.Type.Property (Property(..))

data Action
  = DoNothing
  | Receive PropertyInput
  | ReceiveValidity Boolean
  | Remove
  | UpdateField (Tuple String Int)
  | UpdateParams (Array Parameter)

data Message
  = RemoveProperty Int
  | SendValidity Boolean
  | UpdateProperty (Tuple (Property Value) Int)

data Query a = GetProperty (Property Value -> a)

type PropertyInput =
  { property :: Property Value
  , index :: Int
  }

type State =
  { name :: String
  , value :: String
  , group :: String
  , params :: Array Parameter
  , index :: Int
  , isValid :: Boolean
  }

type Slots =
  ( field :: H.Slot (Const Void) ChildField.Message Int
  , params :: H.Slot (Const Void) ChildParam.Message Int
  )

_field = Proxy :: Proxy "field"
_params = Proxy :: Proxy "params"

initialState :: PropertyInput -> State
initialState { property, index } =
  { name: p.name
  , value: (toOther p.value)
  , group: p.group
  , params: p.params
  , index: index
  , isValid: true
  }
  where
  prop :: forall a. Property a -> { group :: String, name :: String, params :: Array Parameter, value :: a }
  prop (Property pp) = pp

  p = prop property

component :: forall q m. H.Component q PropertyInput Message m
component =
  H.mkComponent
    { initialState: initialState
    , render
    , eval: H.mkEval $ H.defaultEval
        { handleAction = handleAction
        , receive = Just <<< Receive -- redraw
        }
    }

render :: forall m. State -> H.ComponentHTML Action Slots m
render state =
  HH.div [ HP.class_ (HH.ClassName classes.boxed) ]
    [ HH.button
        [ HP.classes [ HH.ClassName classes.buttonRemove, HH.ClassName classes.buttonSmall ]
        , HP.title "remove property"
        , HE.onClick \_ -> Remove
        ]
        []
    , HH.datalist [ HP.id ids.propSuggestions ]
        ( map (\sug -> HH.option [ HP.value sug ] [])
            [ "ANNIVERSARY"
            , "BDAY"
            , "CALADRURI"
            , "CALURI"
            , "CATEGORIES"
            , "CLASS"
            , "CLIENTPIDMAP"
            , "EMAIL"
            , "FBURL"
            , "GENDER"
            , "GEO"
            , "IMPP"
            , "KEY"
            , "KIND"
            , "LABEL"
            , "LANG"
            , "LOGO"
            , "MAILER"
            , "MEMBER"
            , "NAME"
            , "NICKNAME"
            , "NOTE"
            , "PHOTO"
            , "PRODID"
            , "PROFILE"
            , "RELATED"
            , "SORT-STRING"
            , "SOUND"
            , "SOURCE"
            , "TEL"
            , "TITLE"
            , "TZ"
            , "UID"
            , "URL"
            , "XML"
            ]
        )
    , HH.datalist [ HP.id ids.nameSuggestions ]
        ( map (\sug -> HH.option [ HP.value sug ] [])
            [ "CHARSET"
            , "ENCODING"
            , "PID"
            , "PREF"
            , "TYPE"
            , "VALUE"
            ]
        )
    , HH.datalist [ HP.id ids.valueSuggestions ]
        ( map (\sug -> HH.option [ HP.value sug ] [])
            [ "8BIT"
            , "AVI"
            , "B"
            , "BASE64"
            , "BMP"
            , "CELL"
            , "CGM"
            , "DIB"
            , "DOM"
            , "GIF"
            , "HOME"
            , "INTL"
            , "JPEG"
            , "MET"
            , "MPEG"
            , "MPEG2"
            , "PARCEL"
            , "PDF"
            , "PICT"
            , "PMB"
            , "POSTAL"
            , "PREF"
            , "PS"
            , "QTIME"
            , "QUOTED-PRINTABLE"
            , "TEXT"
            , "TIFF"
            , "URI"
            , "URL"
            , "WMF"
            , "WORK"
            ]
        )
    , HH.slot _field 0 (ChildField.component)
        { name: ""
        , value: state.name
        , index: 0
        , isParameter: false
        , isTextArea: false
        , sug: true
        , className: classes.formName
        , validation: Tuple valid validPropertyName
        }
        listenField
    , HH.div_
        [ HH.div [ HP.class_ (HH.ClassName classes.fieldGroup) ]
            [ HH.slot _field 1 (ChildField.component)
                { name: "Property Group"
                , value: state.group
                , index: 1
                , isParameter: false
                , isTextArea: false
                , sug: false
                , className: classes.formField
                , validation: Tuple valid validGroup
                }
                listenField
            ]
        , HH.div [ HP.class_ (HH.ClassName classes.form) ]
            [ HH.slot _field 2 (ChildField.component)
                { name: "Value"
                , value: state.value
                , index: 2
                , isParameter: false
                , sug: false
                , isTextArea: if state.name == "TEL" then false else true
                , className: classes.formField
                , validation: if state.name == "TEL" then Tuple valid validPhoneNumber else Tuple valid valid
                }
                listenField
            ]
        , HH.slot _params 0 (ChildParam.component) { items: state.params } listenParam
        ]
    ]

listenField :: ChildField.Message -> Action
listenField message =
  case message of
    ChildField.RemoveField _ -> DoNothing
    ChildField.SendName _ -> DoNothing
    ChildField.SendValidity b -> ReceiveValidity b
    ChildField.SendValue (Tuple value i) -> UpdateField $ Tuple value i

listenParam :: ChildParam.Message -> Action
listenParam message =
  case message of
    ChildParam.SendParameters params -> UpdateParams params
    ChildParam.SendValidity b -> ReceiveValidity b

handleAction :: forall m. Action -> H.HalogenM State Action Slots Message m Unit
handleAction action = case action of
  DoNothing -> pure unit
  Receive input -> H.modify_ \_ -> initialState input
  ReceiveValidity b -> H.raise $ SendValidity b
  Remove -> do
    state <- H.get
    H.raise (RemoveProperty state.index)
  UpdateField (Tuple value i) -> do
    state <- case i of
      0 -> H.modify \s -> s { name = value }
      1 -> H.modify \s -> s { group = value }
      2 -> H.modify \s -> s { value = value }
      _ -> H.get
    H.raise
      $ UpdateProperty
      $ Tuple (Property { name: state.name, value: Value state.value, group: state.group, params: state.params }) state.index
  UpdateParams params -> do
    state <- H.modify \s -> s { params = params }
    H.raise
      $ UpdateProperty
      $ Tuple (Property { name: state.name, value: Value state.value, group: state.group, params: state.params }) state.index
