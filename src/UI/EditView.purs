-- File Name: EditView.purs
-- Description: Edit view
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 03 May 2023 21:49:12
-- Last Modified: 23 Nov 2023 11:13:20

module UI.EditView where

import Prelude

import Data.Array (deleteAt, mapWithIndex, snoc, updateAt)
import Data.Const (Const)
import Data.Maybe (fromMaybe)
import Data.Tuple (Tuple(..))
import Effect.Aff.Class (class MonadAff)
import Effect.Class.Console (log)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Type.Proxy (Proxy(..))
import UI.Components.AddressForm as ChildAddr
import UI.Components.HelpButton as ChildHelp
import UI.Components.NameForm as ChildName
import UI.Components.PropertyForm as ChildProp
import UI.Style (classes)
import VCF.Type.Address (Address)
import VCF.Type.Common (Value(..), empty, toOther)
import VCF.Type.Name (Name)
import VCF.Type.Property (Property(..))
import VCF.Type.VCard (VCard(..))

data Action
  = GoBack
  | Save
  | AddAddress
  | AddProperty
  | UpdateName (Property Name)
  | UpdateProperty (Tuple (Property Value) Int)
  | UpdateAddress (Tuple (Property Address) Int)
  | UpdateValidity Boolean
  | RemoveAddress Int
  | RemoveProperty Int

data Message
  = GotoList
  | UpdateVCard (Tuple VCard Int)

type EditInput =
  { vcard :: VCard
  , index :: Int
  }

type State =
  { name :: Property Name
  , addrs :: Array (Property Address)
  , props :: Array (Property Value)
  , version :: Number
  , index :: Int
  , isValid :: Boolean
  }

type Slots =
  ( addr :: H.Slot (Const Void) ChildAddr.Message Int
  , help :: H.Slot (Const Void) Unit Int
  , name :: H.Slot (Const Void) ChildName.Message Int
  , prop :: H.Slot (Const Void) ChildProp.Message Int
  )

_addr = Proxy :: Proxy "addr"
_help = Proxy :: Proxy "help"
_name = Proxy :: Proxy "name"
_prop = Proxy :: Proxy "prop"

initialState :: EditInput -> State
initialState { vcard, index } =
  { name: v.name
  , addrs: v.adrs
  , props: v.props
  , version: v.version
  , index: index
  , isValid: true
  }
  where
  toVCard (VCard vv) = vv
  v = toVCard vcard

component :: forall q m. MonadAff m => H.Component q EditInput Message m
component =
  H.mkComponent
    { initialState: initialState
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction }
    }

render :: forall m. MonadAff m => State -> H.ComponentHTML Action Slots m
render state =
  HH.div_
    [ HH.div [ HP.class_ (HH.ClassName classes.navigation) ]
        [ HH.div [ HP.class_ (HH.ClassName classes.navigationLeft) ]
            [ HH.button [ HP.class_ $ HH.ClassName classes.buttonBack, HP.title "back", HE.onClick \_ -> GoBack ] []
            , HH.button [ HP.class_ $ HH.ClassName classes.buttonAddAddr, HP.title "add address", HE.onClick \_ -> AddAddress ] []
            , HH.button [ HP.class_ $ HH.ClassName classes.buttonAddProp, HP.title "add property", HE.onClick \_ -> AddProperty ] []
            , HH.button [ HP.class_ $ HH.ClassName classes.buttonSave, HP.title "save", HE.onClick \_ -> Save ] []
            ]
        , HH.div [ HP.class_ (HH.ClassName classes.navigationRight) ]
            [ HH.slot_ _help 0 (ChildHelp.component) unit ]
        ]
    , HH.span_ [ HH.text $ "Version: " <> (show state.version) ]
    , HH.slot _name 0 (ChildName.component) state.name listenName
    , HH.div_
        (flip mapWithIndex state.addrs \i addr -> HH.slot _addr i (ChildAddr.component) { property: addr, index: i } listenAddr)
    , HH.div_
        (flip mapWithIndex state.props \i prop -> HH.slot _prop i (ChildProp.component) { property: prop, index: i } listenProp)
    ]

listenName :: ChildName.Message -> Action
listenName message = case message of
  ChildName.UpdateName name -> UpdateName name

listenAddr :: ChildAddr.Message -> Action
listenAddr message = case message of
  ChildAddr.RemoveAddress i -> RemoveAddress i
  ChildAddr.SendValidity b -> UpdateValidity b
  ChildAddr.UpdateAddress (Tuple address i) -> UpdateAddress $ Tuple address i

listenProp :: ChildProp.Message -> Action
listenProp message = case message of
  ChildProp.RemoveProperty i -> RemoveProperty i
  ChildProp.SendValidity b -> UpdateValidity b
  ChildProp.UpdateProperty (Tuple property i) -> UpdateProperty $ Tuple property i

handleAction :: forall m. MonadAff m => Action -> H.HalogenM State Action Slots Message m Unit
handleAction action = case action of
  GoBack -> H.raise GotoList
  Save -> do
    state <- H.get
    case state.isValid of
      true ->
        H.raise
          $ UpdateVCard
          $ Tuple
              ( VCard
                  { name: state.name
                  , fn: Property { name: "FN", value: Value $ toOther state.name, group: "", params: [] }
                  , adrs: state.addrs
                  , props: state.props
                  , version: state.version
                  }
              )
              state.index
      false ->
        log "There are still invalid fields"
  AddAddress -> H.modify_ \s -> s { addrs = snoc s.addrs empty }
  AddProperty -> H.modify_ \s -> s { props = snoc s.props (Property { name: "PROPERTY", value: Value "", group: "", params: [] }) }
  UpdateName name -> H.modify_ \s -> s { name = name }
  UpdateProperty (Tuple property i) -> H.modify_ \s -> s { props = fromMaybe s.props $ updateAt i property s.props }
  UpdateAddress (Tuple address i) -> H.modify_ \s -> s { addrs = fromMaybe s.addrs $ updateAt i address s.addrs }
  UpdateValidity b -> H.modify_ \s -> s { isValid = b }
  RemoveAddress i -> H.modify_ \s -> s { addrs = fromMaybe s.addrs $ deleteAt i s.addrs }
  RemoveProperty i -> H.modify_ \s -> s { props = fromMaybe s.props $ deleteAt i s.props }
