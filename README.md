# vCard-editor

vCard-editor is a reader, writer and editor for [vCards](https://en.wikipedia.org/wiki/VCard).  
It should be able to parse every (somewhat) well-formed vCard thrown at it.  
Reader and writer are trying to be backwards compatible with the version 2.1 specification.  
The application performs some (opinionated) actions for convenience, which are listed below.

A running example can be found [here](https://kmijph.codeberg.page/apps/vcard-editor/).

## Reader

The reader is not as strict, to satisfy the requirements for all versions.

- expects `crlf` (\r\n) as line separator
- decodes (quoted-printable) fields (and removes the `ENCODING` parameter)
- does not decode binary fields
- removes duplicate properties
- unescapes escaped characters
- removes indent from multiline values

## Writer

The writer tries to satisfy version 2.1 above all, due to it having the most strict requirements.

- separates lines by `crlf` (→ v2.1 spec)
- generates FN based on the N field (defaults to `PREFIX GIVEN ADD FAMILY SUFFIX`; can be set)
- encodes fields (if invalid ASCII characters are found) and adds the `ENCODING` parameter (default value can be set)
- escapes illegal characters $^*$
- indents multiline values
- removes empty properties and parameters
- wraps lines to a max of 75 characters if they are too long (can be toggled)
- adds a blank line after a binary encoded value (only for BASE64 encoding → v2.1 spec)

$^*$ v2.1 → semi-colon  
$^*$ v3.0 → semi-colon and comma

## Editor

- performs input validation (for most fields)
- does not allow saving of vcards with invalid fields
- does not enforce specific property names
- suggests values for property names, parameter names and parameter values

# UI

The application accepts a single `.vcf` file upon opening.
If the file could be read and parsed, a list view with all the parsed entries will be opened.
If it couldn't be parsed, it will show a parsing error.

## List View

- All values can be selected/unselected by clicking the respective button on the top
- Single entries can be selected/unselected by clicking on the input field on the left side of the name
- Selected entries can be deleted or exported to file by clicking on the respective buttons on the top
- The specifications and this guide can be openened by clicking the `?` button on the top right side
- A new entry can be added by clicking the `+` button
- Entries can be sorted
- A filter can be applied by filling the filter field in the settings (gear button) and hitting ENTER.
  Filtering with an empty filter field will reset the filter.
- Clicking on an entry will send said entry to the edit view

Some settings can be adjusted by clicking on the gear button on the far right, which include:

- `default encoding`: which encoding to choose if the field contains invalid ASCII characters
- `FN generation`: order of the `FN` field, according to [wikipedia](https://en.wikipedia.org/wiki/Personal_name#Name_order)
- `export version`: vCard version to set on export (defaults to 2.1)
- `wrap`: automatically wrap strings that are too long (defaults to `true` → v2.1)

## Edit View

The edit view will show all the parsed fields vertically.

- A generic property can be added by clicking on the `+` button on the top.
- An address can be added by clicking on the address button.
- The vCard can be saved by clicking on the save button.
- Properties (except for `N` and `ADR`) can be renamed by clicking on the property name.
- They can be deleted by clicking the `x` button on the left side of the property name.
- Parameters can be added by clicking on the `+` button on the right side of the `Parameters` field
  and deleted by clicking on the `x` button on the right side of the parameter.

Values will be updated and validated when edited.
In the case of errors, the field will turn red and show a tooltip when hovering over it.

# Building

The entire application is written in purescript and can be built with spago:

```bash
spago bundle
```
