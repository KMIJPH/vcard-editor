-- File Name: Validation.purs
-- Description:
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 28 Okt 2023 15:45:57
-- Last Modified: 08 Nov 2023 19:06:53

module Test.Validation where

import Prelude

import Data.Tuple (Tuple(..))
import Effect (Effect)
import Test.Unit (test)
import Test.Unit.Assert (equal)
import Test.Unit.Main (runTest)
import UI.Validation (messages, validGroup, validParameter, validPhoneNumber, validPropertyName)

validate :: Effect Unit
validate = runTest do
  test "validate" do
    equal (validGroup "B") (Tuple true messages.group)
    equal (validGroup "BBBB") (Tuple false messages.group)
    equal (validPhoneNumber "+0123-4567-89") (Tuple true messages.phoneNumber)
    equal (validPhoneNumber "BBBB") (Tuple false messages.phoneNumber)
    equal (validPropertyName "TEL") (Tuple true messages.propertyName)
    equal (validPropertyName "telephone") (Tuple false messages.propertyName)
    equal (validPropertyName "N") (Tuple false messages.propertyName)
    equal (validPropertyName "ANNIVERSARY") (Tuple true messages.propertyName)
    equal (validParameter "TEL") (Tuple true messages.parameter)
    equal (validParameter "telephone") (Tuple false messages.parameter)
    equal (validParameter "") (Tuple true messages.parameter)
