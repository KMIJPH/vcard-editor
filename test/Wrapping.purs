-- File Name: Wrapping.purs
-- Description:
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 05 Nov 2023 16:59:57
-- Last Modified: 10 Nov 2023 11:20:54

module Test.Wrapping where

import Prelude

line1Str :: String
line1Str =
  "this is a very long line with more than 75 characters. I guess.. I didn't count it ..."

line1Words :: Array String
line1Words =
  [ "this"
  , "is"
  , "a"
  , "very"
  , "long"
  , "line"
  , "with"
  , "more"
  , "than"
  , "75"
  , "characters."
  , "I"
  , "guess.."
  , "I"
  , "didn't"
  , "count"
  , "it"
  , "..."
  ]

line2Str :: String
line2Str =
  "AAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAAAA AAAAAAAAAAAAAAA"
    <> "BBBBBB BBBBBBBBBBBB BBBBBBBBB BBBBBBBBBBBB BBBBBBBBBBBBB"
    <> "CCCCCCCCCCCC CCCCCCCCCCCCCC CCCCCCCCCC CCCCCC CCCCC"
    <> "DDD DDDDDDDD DDDDDDD DDDDDDDD DDDDDDD DDDDDDDD DDDDD"

line2Fold :: String
line2Fold =
  "AAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAAAA AAAAAAAAAAAAAAABB\r\n"
    <> " BBBB BBBBBBBBBBBB BBBBBBBBB BBBBBBBBBBBB BBBBBBBBBBBBBCCCCCCCCCCCC CCCCCCC\r\n"
    <> " CCCCCCC CCCCCCCCCC CCCCCC CCCCCDDD DDDDDDDD DDDDDDD DDDDDDDD DDDDDDD DDDDD\r\n"
    <> " DDD DDDDD"

line3Str :: String
line3Str =
  "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
    <> "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB"
    <> "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"
    <> "DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD"

line3Fold :: String
line3Fold =
  "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBBB\r\n"
    <> " BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBCCCCCCCCCCCCCCCCCCCCCCCCCCC\r\n"
    <> " CCCCCCCCCCCCCCCCCCCCDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD"

multiline1Str :: String
multiline1Str =
  "line1\r\n"
    <> " line2\r\n"
    <> "line3\r\n"
    <> "line4\r\n"

multiline1Split :: Array String
multiline1Split =
  [ "line1"
  , " line2"
  , "line3"
  , "line4"
  , ""
  ]

multiline2Str :: String
multiline2Str =
  "line1\r\n"

multiline2Split :: Array String
multiline2Split =
  [ "line1", "" ]
