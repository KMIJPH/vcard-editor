-- File Name: Encoding.purs
-- Description:
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 01 May 2023 21:05:11
-- Last Modified: 02 Nov 2023 20:49:56

module Test.Encoding where

import Prelude

import Effect (Effect)
import Test.Unit (test)
import Test.Unit.Assert (equal)
import Test.Unit.Main (runTest)
import VCF.Encoding (_decBase64, _decQP, _encBase64, _encQP)
import VCF.Parsers (isValidAscii)

qp1 :: String
qp1 = "s=C3=B6me=20str=C3=BCng"

qp1Should :: String
qp1Should = "söme strüng"

qp2 :: String
qp2 = "=C3=A1=C3=A9'i=C3=B6=C3=BC=C3=BC"

qp2Should :: String
qp2Should = "áé'iöüü"

qp3 :: String
qp3 =
  """=C3=A4sd line
second line
third line
"""

qp3Should :: String
qp3Should =
  """äsd line
second line
third line
"""

base641 :: String
base641 = "c29tZXRoaW5n"

base641Should :: String
base641Should = "something"

base642 :: String
base642 = "dGhpcyBpcyBhIGxvbmdlciBsaW5lIA=="

base642Should :: String
base642Should = "this is a longer line "

decode :: Effect Unit
decode = runTest do
  test "decode" do
    equal (isValidAscii "asd") true
    equal (isValidAscii "äsd") false
    equal (isValidAscii "asd-aa#'") true
    equal (isValidAscii "á") false
    equal (isValidAscii "→") false
    equal (isValidAscii "👌") false

    equal (_decQP qp1) qp1Should
    equal (_encQP qp1Should) qp1
    equal (_decQP qp2) qp2Should
    equal (_encQP qp2Should) qp2
    equal (_decQP qp3) qp3Should
    equal (_decBase64 base641) base641Should
    equal (_encBase64 base641Should) base641
    equal (_decBase64 base642) base642Should
    equal (_encBase64 base642Should) base642
