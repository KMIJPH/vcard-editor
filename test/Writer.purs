-- File Name: Writer.purs
-- Description:
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 31 Okt 2023 12:19:33
-- Last Modified: 06 Nov 2023 11:00:28

module Test.Writer where

import Prelude

import Data.Maybe (Maybe(..))
import Effect (Effect)
import Test.Type.Address as TA
import Test.Type.Name as TN
import Test.Type.Properties as TP
import Test.Type.VCard as TV
import Test.Unit (test)
import Test.Unit.Assert (equal)
import Test.Unit.Main (runTest)
import VCF.Parsers (escape, unEscape)
import VCF.Type.Common (toString)
import VCF.Writer (defaultSettings, write, writeVCards)

writer :: Effect Unit
writer = runTest do
  test "escape" do
    equal (escape """This semicolon; should be escaped""" ';') """This semicolon\; should be escaped"""
    equal (escape """These dots .. should be escaped.""" '.') """These dots \.\. should be escaped\."""
    equal (escape """These dots .. should be escaped. This is already \.""" '.') """These dots \.\. should be escaped\. This is already \."""

  test "unescape" do
    equal (unEscape """\; asd""" ';') "; asd"
    equal (unEscape """\r asd""" 'r') "r asd"
    equal (unEscape """\;\;\;\,""" ';') """;;;\,"""

  test "write" do
    equal (toString Nothing TN.name1Name) TN.name1Written
    equal (toString Nothing TN.name2Name) TN.name2Written
    equal (toString Nothing TN.name3Name) TN.name3Written
    equal (toString Nothing TA.addr1Addr) TA.addr1Written
    equal (toString Nothing TA.addr2Addr) TA.addr2Written
    equal (toString Nothing TP.tel1Prop) TP.tel1Written
    equal (toString Nothing TP.tel2Prop) TP.tel2Written
    equal (toString Nothing TP.multiline1Prop) TP.multiline1Written

    equal (write defaultSettings TV.single1VCard) TV.single1Written
    equal (write defaultSettings TV.single2VCard) TV.single2Written
    equal (write defaultSettings TV.single3VCard) TV.single3Written

    equal (writeVCards [ TV.single2VCard, TV.single3VCard ] defaultSettings)
      (TV.single2Written <> "\r\n" <> TV.single3Written)
