-- File Name: VCard.purs
-- Description: vCard examples
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 30 Apr 2023 18:57:25
-- Last Modified: 21 Dez 2023 12:01:40

module Test.Type.VCard where

import Prelude

import Test.Type.Address as TA
import VCF.Type.Address (Address(..))
import VCF.Type.Common (Parameter(..), Value(..))
import VCF.Type.Name (Name(..))
import VCF.Type.Property (Property(..))
import VCF.Type.VCard (VCard(..))

single1Str :: String
single1Str = "BEGIN:VCARD\r\n"
  <> "VERSION:2.1\r\n"
  <> "FN:Fred Friday\r\n"
  <> "N:Friday;Fred\r\n"
  <> "A.TEL:1234567890\r\n"
  <> "B.BDAY:1995-04-15\r\n"
  <> "B.NOTE:A note\r\n"
  <> "X-CELL:1234567890\r\n"
  <> "ADR;DOM;HOME:P.O. Box 101;Suite 101;123 Main Street;Any Town;CA;91921-1234;\r\n"
  <> "END:VCARD"

single1Written :: String
single1Written = "BEGIN:VCARD\r\n"
  <> "VERSION:2.1\r\n"
  <> "N:Friday;Fred;;;\r\n"
  <> "FN:Fred Friday\r\n"
  <> "A.TEL:1234567890\r\n"
  <> "B.BDAY:1995-04-15\r\n"
  <> "B.NOTE:A note\r\n"
  <> "X-CELL:1234567890\r\n"
  <> "ADR;DOM;HOME:P.O. Box 101;Suite 101;123 Main Street;Any Town;CA;91921-1234;\r\n"
  <> "END:VCARD"

single1VCard :: VCard
single1VCard =
  VCard
    { adrs:
        [ Property
            { name: "ADR"
            , value: TA.addr1Addr
            , group: ""
            , params:
                [ Parameter { param: "", value: "DOM" }
                , Parameter { param: "", value: "HOME" }
                ]
            }
        ]
    , name: Property
        { name: "N"
        , value: Name { family: "Friday", given: "Fred", add: "", prefix: "", suffix: "" }
        , group: ""
        , params: []
        }
    , fn: Property { name: "FN", value: Value "Fred Friday", group: "", params: [] }
    , props:
        [ Property { name: "TEL", value: Value "1234567890", group: "A", params: [] }
        , Property { name: "BDAY", value: Value "1995-04-15", group: "B", params: [] }
        , Property { name: "NOTE", value: Value "A note", group: "B", params: [] }
        , Property { name: "X-CELL", value: Value "1234567890", group: "", params: [] }
        ]
    , version: 2.1
    }

single2Str :: String
single2Str =
  "BEGIN:VCARD\r\n"
    <> "VERSION:2.1\r\n"
    <> "N:Mustermann;Erika;;Dr.;\r\n"
    <> "FN:Dr. Erika Mustermann\r\n"
    <> "ORG:Wikimedia\r\n"
    <> "ROLE:Kommunikation\r\n"
    <> "TITLE:Redaktion & Gestaltung\r\n"
    <> "PHOTO;JPEG:http://commons.wikimedia.org/wiki/File:Erika_Mustermann_2010.jpg\r\n"
    <> "TEL;WORK;VOICE:(0221) 9999123\r\n"
    <> "TEL;HOME;VOICE:(0221) 1234567\r\n"
    <> "ADR;HOME:;;Heidestrasse 17;Koeln;;51147;Deutschland\r\n"
    <> "EMAIL;PREF;INTERNET:erika@mustermann.de\r\n"
    <> "REV:20140301T221110Z\r\n"
    <> "END:VCARD"

single2Written :: String
single2Written =
  "BEGIN:VCARD\r\n"
    <> "VERSION:2.1\r\n"
    <> "N:Mustermann;Erika;;Dr.;\r\n"
    <> "FN:Dr. Erika Mustermann\r\n"
    <> "ORG:Wikimedia\r\n"
    <> "ROLE:Kommunikation\r\n"
    <> "TITLE:Redaktion & Gestaltung\r\n"
    <> "PHOTO;JPEG:http://commons.wikimedia.org/wiki/File:Erika_Mustermann_2010.jp\r\n"
    <> " g\r\n"
    <> "TEL;WORK;VOICE:(0221) 9999123\r\n"
    <> "TEL;HOME;VOICE:(0221) 1234567\r\n"
    <> "EMAIL;PREF;INTERNET:erika@mustermann.de\r\n"
    <> "REV:20140301T221110Z\r\n"
    <> "ADR;HOME:;;Heidestrasse 17;Koeln;;51147;Deutschland\r\n"
    <> "END:VCARD"

single2VCard :: VCard
single2VCard =
  VCard
    { adrs:
        [ Property
            { name: "ADR"
            , value: Address { adr: "", ctry: "Deutschland", ext: "", loc: "Koeln", postal: "51147", reg: "", str: "Heidestrasse 17" }
            , group: ""
            , params: [ Parameter { param: "", value: "HOME" } ]
            }
        ]
    , name: Property
        { name: "N"
        , value: Name { add: "", family: "Mustermann", given: "Erika", prefix: "Dr.", suffix: "" }
        , group: ""
        , params: []
        }
    , fn: Property { name: "FN", value: Value "Dr. Erika Mustermann", group: "", params: [] }
    , props:
        [ Property { name: "ORG", value: Value "Wikimedia", group: "", params: [] }
        , Property { name: "ROLE", value: Value "Kommunikation", group: "", params: [] }
        , Property { name: "TITLE", value: Value "Redaktion & Gestaltung", group: "", params: [] }
        , Property
            { name: "PHOTO"
            , value: Value "http://commons.wikimedia.org/wiki/File:Erika_Mustermann_2010.jpg"
            , group: ""
            , params: [ Parameter { param: "", value: "JPEG" } ]
            }
        , Property
            { name: "TEL"
            , value: Value "(0221) 9999123"
            , group: ""
            , params:
                [ Parameter { param: "", value: "WORK" }
                , Parameter { param: "", value: "VOICE" }
                ]
            }
        , Property
            { name: "TEL"
            , value: Value "(0221) 1234567"
            , group: ""
            , params:
                [ Parameter { param: "", value: "HOME" }
                , Parameter { param: "", value: "VOICE" }
                ]
            }
        , Property
            { name: "EMAIL"
            , value: Value "erika@mustermann.de"
            , group: ""
            , params:
                [ Parameter { param: "", value: "PREF" }
                , Parameter { param: "", value: "INTERNET" }
                ]
            }
        , Property { name: "REV", value: Value "20140301T221110Z", group: "", params: [] }
        ]
    , version: 2.1
    }

single3Str :: String
single3Str =
  "BEGIN:VCARD\r\n"
    <> "VERSION:2.1\r\n"
    <> "N:Gump;Forrest;;Mr.\r\n"
    <> "FN:Forrest Gump\r\n"
    <> "ORG:Bubba Gump Shrimp Co.\r\n"
    <> "TITLE:Shrimp Man\r\n"
    <> "PHOTO;GIF:http://www.example.com/dir_photos/my_photo.gif\r\n"
    <> "TEL;WORK;VOICE:(111) 555-1212\r\n"
    <> "TEL;HOME;VOICE:(404) 555-1212\r\n"
    <> "ADR;WORK;PREF:;;100 Waters Edge;Baytown;LA;30314;United States of America\r\n"
    <> "LABEL;WORK;PREF;ENCODING=QUOTED-PRINTABLE;CHARSET=UTF-8:\r\n"
    <> " 100 Waters Edge\r\n"
    <> "  Baytown, LA 30314\r\n"
    <> "  United States of America\r\n"
    <> "ADR;HOME:;;42 Plantation St.;Baytown;LA;30314;United States of America\r\n"
    <> "LABEL;HOME;ENCODING=QUOTED-PRINTABLE;CHARSET=UTF-8:\r\n"
    <> " 42 Plantation St.\r\n"
    <> "  Baytown, LA 30314\r\n"
    <> "  United States of America\r\n"
    <> "EMAIL:forrestgump@example.com\r\n"
    <> "REV:20080424T195243Z\r\n"
    <> "END:VCARD"

single3Written :: String
single3Written =
  "BEGIN:VCARD\r\n"
    <> "VERSION:2.1\r\n"
    <> "N:Gump;Forrest;;Mr.;\r\n"
    <> "FN:Mr. Forrest Gump\r\n"
    <> "ORG:Bubba Gump Shrimp Co.\r\n"
    <> "TITLE:Shrimp Man\r\n"
    <> "PHOTO;GIF:http://www.example.com/dir_photos/my_photo.gif\r\n"
    <> "TEL;WORK;VOICE:(111) 555-1212\r\n"
    <> "TEL;HOME;VOICE:(404) 555-1212\r\n"
    <> "LABEL;WORK;PREF;CHARSET=UTF-8:\r\n"
    <> "100 Waters Edge\r\n"
    <> " Baytown\\, LA 30314\r\n"
    <> " United States of America\r\n"
    <> "LABEL;HOME;CHARSET=UTF-8:\r\n"
    <> "42 Plantation St.\r\n"
    <> " Baytown\\, LA 30314\r\n"
    <> " United States of America\r\n"
    <> "EMAIL:forrestgump@example.com\r\n"
    <> "REV:20080424T195243Z\r\n"
    <> "ADR;WORK;PREF:;;100 Waters Edge;Baytown;LA;30314;United States of America\r\n"
    <> "ADR;HOME:;;42 Plantation St.;Baytown;LA;30314;United States of America\r\n"
    <> "END:VCARD"

single3VCard :: VCard
single3VCard =
  VCard
    { adrs:
        [ Property
            { group: ""
            , name: "ADR"
            , params: [ Parameter { param: "", value: "WORK" }, Parameter { param: "", value: "PREF" } ]
            , value: Address { adr: "", ctry: "United States of America", ext: "", loc: "Baytown", postal: "30314", reg: "LA", str: "100 Waters Edge" }
            }
        , Property
            { group: ""
            , name: "ADR"
            , params: [ Parameter { param: "", value: "HOME" } ]
            , value: Address { adr: "", ctry: "United States of America", ext: "", loc: "Baytown", postal: "30314", reg: "LA", str: "42 Plantation St." }
            }
        ]
    , name: Property
        { group: ""
        , name: "N"
        , params: []
        , value: Name { add: "", family: "Gump", given: "Forrest", prefix: "Mr.", suffix: "" }
        }
    , fn: Property { group: "", name: "FN", params: [], value: Value "Mr. Forrest Gump" }
    , props:
        [ Property { group: "", name: "ORG", params: [], value: Value "Bubba Gump Shrimp Co." }
        , Property { group: "", name: "TITLE", params: [], value: Value "Shrimp Man" }
        , Property
            { group: ""
            , name: "PHOTO"
            , params: [ Parameter { param: "", value: "GIF" } ]
            , value: Value "http://www.example.com/dir_photos/my_photo.gif"
            }
        , Property
            { group: ""
            , name: "TEL"
            , params: [ Parameter { param: "", value: "WORK" }, Parameter { param: "", value: "VOICE" } ]
            , value: Value "(111) 555-1212"
            }
        , Property
            { group: ""
            , name: "TEL"
            , params: [ Parameter { param: "", value: "HOME" }, Parameter { param: "", value: "VOICE" } ]
            , value: Value "(404) 555-1212"
            }
        , Property
            { group: ""
            , name: "LABEL"
            , params:
                [ Parameter { param: "", value: "WORK" }
                , Parameter { param: "", value: "PREF" }
                , Parameter { param: "CHARSET", value: "UTF-8" }
                ]
            , value: Value "\r\n100 Waters Edge\r\n Baytown, LA 30314\r\n United States of America"
            }
        , Property
            { group: ""
            , name: "LABEL"
            , params:
                [ Parameter { param: "", value: "HOME" }
                , Parameter { param: "CHARSET", value: "UTF-8" }
                ]
            , value: Value "\r\n42 Plantation St.\r\n Baytown, LA 30314\r\n United States of America"
            }
        , Property { group: "", name: "EMAIL", params: [], value: Value "forrestgump@example.com" }
        , Property { group: "", name: "REV", params: [], value: Value "20080424T195243Z" }
        ]
    , version: 2.1
    }

single4Str :: String
single4Str =
  "BEGIN:VCARD\r\n"
    <> "VERSION:2.1\r\n"
    <> "N;CHARSET=UTF-8;ENCODING=8BIT:Söme;Näme;;;\r\n"
    <> "FN;ENCODING=8BIT:Näme Söme\r\n"
    <> "ADR;ENCODING=8BIT:Straße 4;;;;;;\r\n"
    <> "END:VCARD"

single4VCard :: VCard
single4VCard =
  VCard
    { adrs:
        [ Property
            { group: ""
            , name: "ADR"
            , params: [ Parameter { param: "ENCODING", value: "8BIT" } ]
            , value: Address { adr: "Straße 4", ctry: "", ext: "", loc: "", postal: "", reg: "", str: "" }
            }
        ]
    , fn: Property
        { group: ""
        , name: "FN"
        , params: [ Parameter { param: "CHARSET", value: "UTF-8" }, Parameter { param: "ENCODING", value: "8BIT" } ]
        , value: Value "Näme Söme"
        }
    , name:
        Property
          { group: ""
          , name: "N"
          , params: [ Parameter { param: "CHARSET", value: "UTF-8" }, Parameter { param: "ENCODING", value: "8BIT" } ]
          , value: Name { add: "", family: "Söme", given: "Näme", prefix: "", suffix: "" }
          }
    , props: []
    , version: 2.1
    }

multiple1Str :: String
multiple1Str = single2Str <> single3Str

nested1Str :: String
nested1Str =
  "BEGIN:VCARD\r\n"
    <> "VERSION:2.1\r\n"
    <> "X-DL;Design Work Group:List Item 1;List Item 2;List Item 3\r\n"
    <> "BEGIN:VCARD\r\n"
    <> "N:John Smith\r\n"
    <> "UID:List Item 1\r\n"
    <> "TEL:+1-213-555-1111\r\n"
    <> "END:VCARD\r\n"
    <> "BEGIN:VCARD\r\n"
    <> "N:I. M. Big\r\n"
    <> "UID:List Item 2\r\n"
    <> "TEL:+1-213-555-9999\r\n"
    <> "END:VCARD\r\n"
    <> "BEGIN:VCARD\r\n"
    <> "N:Jane Doe\r\n"
    <> "UID:List Item 3\r\n"
    <> "TEL:+1-213-555-5555\r\n"
    <> "END:VCARD\r\n"
    <> "END:VCARD"

nested1VCards :: Array VCard
nested1VCards =
  [ VCard
      { adrs: []
      , name: Property
          { name: "N"
          , value: Name { add: "", family: "John Smith", given: "", prefix: "", suffix: "" }
          , group: ""
          , params: []
          }
      , fn: Property
          { name: "FN"
          , value: Value "John Smith"
          , group: ""
          , params: []
          }
      , props:
          [ Property { name: "UID", value: Value "List Item 1", group: "", params: [] }
          , Property { name: "TEL", value: Value "+1-213-555-1111", group: "", params: [] }
          , Property { name: "X-DL", value: Value "List Item 1;List Item 2;List Item 3", group: "", params: [ Parameter { param: "", value: "Design Work Group" } ] }
          ]
      , version: 2.1
      }
  , VCard
      { adrs: []
      , name: Property
          { name: "N"
          , value: Name { add: "", family: "I. M. Big", given: "", prefix: "", suffix: "" }
          , group: ""
          , params: []
          }
      , fn: Property
          { name: "FN"
          , value: Value "I. M. Big"
          , group: ""
          , params: []
          }
      , props:
          [ Property { name: "UID", value: Value "List Item 2", group: "", params: [] }
          , Property { name: "TEL", value: Value "+1-213-555-9999", group: "", params: [] }
          , Property { name: "X-DL", value: Value "List Item 1;List Item 2;List Item 3", group: "", params: [ Parameter { param: "", value: "Design Work Group" } ] }
          ]
      , version: 2.1
      }
  , VCard
      { adrs: []
      , name: Property
          { name: "N"
          , value: Name { add: "", family: "Jane Doe", given: "", prefix: "", suffix: "" }
          , group: ""
          , params: []
          }
      , fn: Property
          { name: "FN"
          , value: Value "Jane Doe"
          , group: ""
          , params: []
          }
      , props:
          [ Property { name: "UID", value: Value "List Item 3", group: "", params: [] }
          , Property { name: "TEL", value: Value "+1-213-555-5555", group: "", params: [] }
          , Property { name: "X-DL", value: Value "List Item 1;List Item 2;List Item 3", group: "", params: [ Parameter { param: "", value: "Design Work Group" } ] }
          ]
      , version: 2.1
      }
  ]

multiline1Str :: String
multiline1Str =
  "BEGIN:VCARD\r\n"
    <> "VERSION:2.1\r\n"
    <> "N:Last;First;;;\r\n"
    <> "FN:First Last\r\n"
    <> "TEL;X-Mobil:123456789\r\n"
    <> "TEL;HOME:9875654321\r\n"
    <> "PHOTO:aaaaaaaaaaaaaaaaaaaaaaaaa\r\n"
    <> " bbbbbbbbbbbbbbbbbbbbb\r\n"
    <> " ccccccccccccccccccccc\r\n"
    <> ""
    <> "END:VCARD\r\n"
    <> "BEGIN:VCARD\r\n"
    <> "VERSION:2.1\r\n"
    <> "N:;Stefan;;;\r\n"
    <> "FN:Stefan\r\n"
    <> "TEL;CELL:123456789\r\n"
    <> "END:VCARD"

multiline1VCards :: Array VCard
multiline1VCards =
  [ VCard
      { adrs: []
      , fn: Property { group: "", name: "FN", params: [], value: Value "First Last" }
      , name: Property { group: "", name: "N", params: [], value: Name { add: "", family: "Last", given: "First", prefix: "", suffix: "" } }
      , props:
          [ Property { group: "", name: "TEL", params: [ Parameter { param: "", value: "X-Mobil" } ], value: Value "123456789" }
          , Property { group: "", name: "TEL", params: [ Parameter { param: "", value: "HOME" } ], value: Value "9875654321" }
          , Property { group: "", name: "PHOTO", params: [], value: Value "aaaaaaaaaaaaaaaaaaaaaaaaa\r\nbbbbbbbbbbbbbbbbbbbbb\r\nccccccccccccccccccccc" }
          ]
      , version: 2.1
      }
  , VCard
      { adrs: []
      , fn: Property { group: "", name: "FN", params: [], value: Value "Stefan" }
      , name: Property { group: "", name: "N", params: [], value: Name { add: "", family: "", given: "Stefan", prefix: "", suffix: "" } }
      , props: [ Property { group: "", name: "TEL", params: [ Parameter { param: "", value: "CELL" } ], value: Value "123456789" } ]
      , version: 2.1
      }
  ]
