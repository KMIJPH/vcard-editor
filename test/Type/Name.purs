-- File Name: Name.purs
-- Description: Name examples
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 30 Apr 2023 16:38:10
-- Last Modified: 10 Nov 2023 11:18:52

module Test.Type.Name where

import VCF.Type.Common (Value(..))
import VCF.Type.Name (Name(..))
import VCF.Type.Property (Property(..))

name1Str :: String
name1Str = "A.N:Last\\; Name;First Name;\r\n"

name1Written :: String
name1Written = "Last\\; Name;First Name;;;"

name1Prop :: Property Value
name1Prop =
  Property { name: "N", value: Value "Last\\; Name;First Name;", group: "A", params: [] }

name1Name :: Name
name1Name = Name { family: "Last; Name", given: "First Name", add: "", prefix: "", suffix: "" }

name1Escaped :: Name
name1Escaped = Name { family: "Last\\; Name", given: "First Name", add: "", prefix: "", suffix: "" }

name2Str :: String
name2Str = "B.N:Public;John;Quinlan;Mr.;Esq.\r\n"

name2Written :: String
name2Written = "Public;John;Quinlan;Mr.;Esq."

name2Prop :: Property Value
name2Prop =
  Property { name: "N", value: Value "Public;John;Quinlan;Mr.;Esq.", group: "B", params: [] }

name2Name :: Name
name2Name = Name { family: "Public", given: "John", add: "Quinlan", prefix: "Mr.", suffix: "Esq." }

name3Str :: String
name3Str = "N:Veni, Vidi, Vici;The Restaurant.\r\n"

name3Written :: String
name3Written = "Veni\\, Vidi\\, Vici;The Restaurant.;;;"

name3Prop :: Property Value
name3Prop =
  Property { name: "N", value: Value "Veni, Vidi, Vici;The Restaurant.", group: "", params: [] }

name3Name :: Name
name3Name = Name { family: "Veni, Vidi, Vici", given: "The Restaurant.", add: "", prefix: "", suffix: "" }

-- FN
name1 :: Name
name1 = Name { family: "Public", given: "John", add: "Q.", prefix: "Mr.", suffix: "Esq." }

name2 :: Name
name2 = Name { family: "Tyler", given: "Ann", add: "", prefix: "Dr.", suffix: "" }

name3 :: Name
name3 = Name { family: "Friday", given: "Ted", add: "A.", prefix: "", suffix: "" }
