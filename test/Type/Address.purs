-- File Name: Address.purs
-- Description: Address examples
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 30 Apr 2023 18:57:25
-- Last Modified: 10 Nov 2023 11:18:27

module Test.Type.Address where

import VCF.Type.Address (Address(..))
import VCF.Type.Common (Parameter(..), Value(..))
import VCF.Type.Property (Property(..))

addr1Str :: String
addr1Str = "ADR;DOM;HOME:P.O. Box 101;Suite 101;123 Main Street;Any Town;CA;91921-1234;\r\n"

addr1Written :: String
addr1Written = "P.O. Box 101;Suite 101;123 Main Street;Any Town;CA;91921-1234;"

addr1Prop :: Property Value
addr1Prop =
  Property
    { name: "ADR"
    , value: Value "P.O. Box 101;Suite 101;123 Main Street;Any Town;CA;91921-1234;"
    , group: ""
    , params:
        [ Parameter { param: "", value: "DOM" }
        , Parameter { param: "", value: "HOME" }
        ]
    }

addr1Addr :: Address
addr1Addr =
  Address
    { adr: "P.O. Box 101"
    , ext: "Suite 101"
    , str: "123 Main Street"
    , loc: "Any Town"
    , reg: "CA"
    , postal: "91921-1234"
    , ctry: ""
    }

addr2Str :: String
addr2Str = "N.ADR;DOM;WORK;HOME;POSTAL:P.O. Box 101;;;Any Town;CA;91921-1234;\r\n"

addr2Written :: String
addr2Written = "P.O. Box 101;;;Any Town;CA;91921-1234;"

addr2Prop :: Property Value
addr2Prop =
  Property
    { name: "ADR"
    , value: Value "P.O. Box 101;;;Any Town;CA;91921-1234;"
    , group: "N"
    , params:
        [ Parameter { param: "", value: "DOM" }
        , Parameter { param: "", value: "WORK" }
        , Parameter { param: "", value: "HOME" }
        , Parameter { param: "", value: "POSTAL" }
        ]
    }

addr2Addr :: Address
addr2Addr =
  Address
    { adr: "P.O. Box 101"
    , ext: ""
    , str: ""
    , loc: "Any Town"
    , reg: "CA"
    , postal: "91921-1234"
    , ctry: ""
    }
