-- File Name: Properties.purs
-- Description: Property field examples
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 30 Apr 2023 18:57:25
-- Last Modified: 10 Nov 2023 11:53:04

module Test.Type.Properties where

import Prelude

import VCF.Type.Common (Parameter(..), Value(..))
import VCF.Type.Property (Property(..))

tel1Str :: String
tel1Str = "TEL:+1-213-555-1234\r\n"

tel1Written :: String
tel1Written = "TEL:+1-213-555-1234"

tel1Prop :: Property Value
tel1Prop =
  Property { name: "TEL", value: Value "+1-213-555-1234", group: "", params: [] }

tel2Str :: String
tel2Str = "TEL;X-MAIN;ENCODING=BASE64:12135551234\r\n"

tel2Written :: String
tel2Written = "TEL;X-MAIN;ENCODING=BASE64:12135551234\r\n"

tel2Prop :: Property Value
tel2Prop =
  Property
    { name: "TEL"
    , value: Value "12135551234"
    , group: ""
    , params:
        [ Parameter { param: "", value: "X-MAIN" }
        , Parameter { param: "ENCODING", value: "BASE64" }
        ]
    }

tel3Str :: String
tel3Str = "F.TEL;CELL;VOICE:+423029931023\r\n"

tel3Prop :: Property Value
tel3Prop =
  Property
    { name: "TEL"
    , value: Value "+423029931023"
    , group: "F"
    , params:
        [ Parameter { param: "", value: "CELL" }
        , Parameter { param: "", value: "VOICE" }
        ]
    }

tel4Str :: String
tel4Str = "TEL;LANGUAGE=fr-CA:+423029931023\r\n"

tel4Prop :: Property Value
tel4Prop =
  Property
    { name: "TEL"
    , value: Value "+423029931023"
    , group: ""
    , params: [ Parameter { param: "LANGUAGE", value: "fr-CA" } ]
    }

name1Decoded :: Property Value
name1Decoded =
  Property
    { name: "N"
    , value: Value "Rami =C3=84yh=C3=B6"
    , group: ""
    , params: [ Parameter { param: "ENCODING", value: "QUOTED-PRINTABLE" } ]
    }

name1Prop :: Property Value
name1Prop =
  Property
    { name: "N"
    , value: Value "Rami Äyhö"
    , group: ""
    , params: []
    }

agent1Str :: String
agent1Str =
  "AGENT:BEGIN:VCARD\n"
    <> "FN:Joe Friday\n"
    <> "TEL:+1-919-555-7878\n"
    <> "TITLE:Area Administrator\\, Assistant\n"
    <> "EMAIL\\;TYPE=INTERN\n"
    <> "ET:jfriday@host.com\nEND:VCARD\r\n"

agent1Prop :: Property Value
agent1Prop = Property
  { name: "AGENT"
  , value: Value (
    "BEGIN:VCARD\n"
    <> "FN:Joe Friday\n"
    <> "TEL:+1-919-555-7878\n"
    <> "TITLE:Area Administrator\\, Assistant\n"
    <> "EMAIL\\;TYPE=INTERN\n"
    <> "ET:jfriday@host.com\nEND:VCARD")
  , group: ""
  , params: []
  }

multiline1Str :: String
multiline1Str = "LABEL;ENCODING=QUOTED-PRINTABLE:123 Winding Way\r\n  Any Town, CA 12345\r\n  USA\r\n"

multiline1Written :: String
multiline1Written = "LABEL;ENCODING=QUOTED-PRINTABLE:123=20Winding=20Way=0D=0A=20Any=20Town,=20CA=2012345=0D=0A=20USA"

multiline1Prop :: Property Value
multiline1Prop =
  Property
    { name: "LABEL"
    , value: Value "123 Winding Way\r\n Any Town, CA 12345\r\n USA"
    , group: ""
    , params: [ Parameter { param: "ENCODING", value: "QUOTED-PRINTABLE" } ]
    }

multiline2Str :: String
multiline2Str = "PHOTO;ENCODING=BASE64;TYPE=GIF:\r\n"
  <> " R0lGODdhfgA4AOYAAAAAAK+vr62trVIxa6WlpZ+fnzEpCEpzlAha/0Kc74+PjyGM\r\n"
  <> " SuecKRhrtX9/fzExORBSjCEYCGtra2NjYyF7nDGE50JrhAg51qWtOTl7vee1MWu1\r\n"
  <> " 50o5e3PO/3sxcwAx/4R7GBgQOcDAwFoAQt61hJyMGHuUSpRKIf8A/wAY54yMjHtz\r\n"

multiline2Prop :: Property Value
multiline2Prop =
  Property
    { name: "PHOTO"
    , value: Value
        ( "\r\nR0lGODdhfgA4AOYAAAAAAK+vr62trVIxa6WlpZ+fnzEpCEpzlAha/0Kc74+PjyGM\r\n"
            <> "SuecKRhrtX9/fzExORBSjCEYCGtra2NjYyF7nDGE50JrhAg51qWtOTl7vee1MWu1\r\n"
            <> "50o5e3PO/3sxcwAx/4R7GBgQOcDAwFoAQt61hJyMGHuUSpRKIf8A/wAY54yMjHtz"
        )
    , group: ""
    , params:
        [ Parameter { param: "ENCODING", value: "BASE64" }
        , Parameter { param: "TYPE", value: "GIF" }
        ]
    }

multiline3Str :: String
multiline3Str = "SOUND;WAVE;BASE64:\r\n"
  <> " UklGRhAsAABXQVZFZm10IBAAAAABAAEAESsAABErAAABAAgAZGF0YesrAACAg4eC\r\n"
  <> " eXR4e3uAhoiIiYmKjIiDfnx5eX6CgoKEhYWDenV5fH6BhISGiIiDfHZ2eXt/hIiK\r\n"
  <> " jY2IhH12d3Vyc3uDiIiFf3l7fn18eXl+houFf319fnyAgHl5eoCIiISChIeAfnt2\r\n"

multiline3Prop :: Property Value
multiline3Prop =
  Property
    { name: "SOUND"
    , value: Value
        ( "\r\nUklGRhAsAABXQVZFZm10IBAAAAABAAEAESsAABErAAABAAgAZGF0YesrAACAg4eC\r\n"
            <> "eXR4e3uAhoiIiYmKjIiDfnx5eX6CgoKEhYWDenV5fH6BhISGiIiDfHZ2eXt/hIiK\r\n"
            <> "jY2IhH12d3Vyc3uDiIiFf3l7fn18eXl+houFf319fnyAgHl5eoCIiISChIeAfnt2"
        )
    , group: ""
    , params:
        [ Parameter { param: "", value: "WAVE" }
        , Parameter { param: "", value: "BASE64" }
        ]
    }
