-- File Name: Main.purs
-- Description: vCard tests
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 16 Mar 2023 07:24:24
-- Last Modified: 02 Nov 2023 12:46:03

module Test.Main where

import Prelude

import Effect (Effect)
import Test.Encoding (decode)
import Test.Reader (reader)
import Test.Validation (validate)
import Test.Writer (writer)

main :: Effect Unit
main = do
  decode
  reader
  writer
  validate
