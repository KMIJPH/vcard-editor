-- File Name: Reader.purs
-- Description: Reader tests
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 02 May 2023 16:10:43
-- Last Modified: 21 Dez 2023 11:59:59

module Test.Reader where

import Prelude

import Data.Either (Either(..))
import Effect (Effect)
import Parsing (Parser, runParser)
import Parsing.Combinators.Array (many)
import Test.Type.Address as TA
import Test.Type.Name as TN
import Test.Type.Properties as TP
import Test.Type.VCard as TV
import Test.Unit (Test, failure, test)
import Test.Unit.Assert (equal)
import Test.Unit.Main (runTest)
import Test.Wrapping as TW
import VCF.Encoding (Encodings(..))
import VCF.Parsers (splitLines, version, words, wrapLine)
import VCF.Reader (nested, single, known)
import VCF.Type.Common (Value(..), fromPrimitive, toOther)

-- | test a single parser
testParser :: forall a. Eq a => Show a => String -> Parser String a -> a -> Test
testParser str p equ =
  case runParser str p of
    Left e -> failure $ "at " <> str <> ": " <> show e
    Right x -> equal x equ

reader :: Effect Unit
reader = runTest do
  test "wrap" do
    testParser TW.line1Str words TW.line1Words
    testParser "aaaaaaaaaaaaaa" words [ "aaaaaaaaaaaaaa" ]
    testParser TW.line2Str (wrapLine 75) TW.line2Fold
    testParser TW.line3Str (wrapLine 75) TW.line3Fold
    testParser TW.multiline1Str splitLines TW.multiline1Split
    testParser TW.multiline2Str splitLines TW.multiline2Split

  test "name" do
    testParser TN.name1Str (known "N") TN.name1Prop
    equal (fromPrimitive QP (Value "Last\\; Name;First Name;")) TN.name1Name

    testParser TN.name2Str (known "N") TN.name2Prop
    equal (fromPrimitive QP (Value "Public;John;Quinlan;Mr.;Esq.")) TN.name2Name

    testParser TN.name3Str (known "N") TN.name3Prop
    equal (fromPrimitive QP (Value "Veni, Vidi, Vici;The Restaurant.")) TN.name3Name

    equal (fromPrimitive QP TP.name1Decoded) TP.name1Prop

  test "fn" do
    equal (toOther TN.name1) "Mr. John Q. Public Esq."
    equal (toOther TN.name2) "Dr. Ann Tyler"
    equal (toOther TN.name3) "Ted A. Friday"

  test "version" do
    testParser "VERSION:2.1\r\n" (version) 2.1
    testParser "VERSION:3.0\r\n" (version) 3.0
    testParser "VERSION:4.0\r\n" (version) 4.0
    testParser "VERSION:5.0\r\n" (version) 5.0

  test "tel" do
    testParser TP.tel1Str (known "TEL") TP.tel1Prop
    testParser TP.tel2Str (known "TEL") TP.tel2Prop
    testParser TP.tel3Str (known "TEL") TP.tel3Prop
    testParser TP.tel4Str (known "TEL") TP.tel4Prop

  test "agent" do
    testParser TP.agent1Str (known "AGENT") TP.agent1Prop

  test "multiline" do
    testParser TP.multiline1Str (known "LABEL") TP.multiline1Prop
    testParser TP.multiline2Str (known "PHOTO") TP.multiline2Prop
    testParser TP.multiline3Str (known "SOUND") TP.multiline3Prop

  test "adr" do
    testParser TA.addr1Str (known "ADR") TA.addr1Prop
    equal (fromPrimitive QP (Value "P.O. Box 101;Suite 101;123 Main Street;Any Town;CA;91921-1234;")) TA.addr1Addr

    testParser TA.addr2Str (known "ADR") TA.addr2Prop
    equal (fromPrimitive QP (Value "P.O. Box 101;;;Any Town;CA;91921-1234;")) TA.addr2Addr

  test "single" do
    testParser TV.single1Str (single true) TV.single1VCard
    testParser TV.single2Str (single true) TV.single2VCard
    testParser TV.single3Str (single true) TV.single3VCard
    testParser TV.single4Str (single true) TV.single4VCard
    testParser TV.multiline1Str (many $ single true) TV.multiline1VCards

  test "multiple" do
    testParser TV.multiple1Str (many (single false)) [ TV.single2VCard, TV.single3VCard ]

  test "nested" do
    testParser TV.nested1Str nested TV.nested1VCards
